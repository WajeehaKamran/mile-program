package com.mile.pushNotifications;

public interface ApplicationConstants {

	static final String APP_SERVER_URL = "http://api.mile.org/gcm.php";
	static final String GOOGLE_PROJ_ID = "940768834907";
	static final String MSG_KEY = "m";

}
  