package com.mile.adapters;

import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mile.android.R;
import com.mile.fragments.DocumentFragment;
import com.mile.fragments.GalleryFragment;
import com.mile.fragments.ProgramDocumentFragment;
import com.mile.fragments.SpecialDocumentFragment;
import com.mile.model.ProgramLibararyModel;
import com.mile.utility.ImageLoader;
import com.mile.utility.Utility;

public class ProgramLibrarayAdapter extends BaseAdapter {
	public List<ProgramLibararyModel> list;
	protected Activity parentActivity;
	Context context;
	public ImageLoader imageLoader;
    
	public ProgramLibrarayAdapter(Activity activity, List<ProgramLibararyModel> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}
	
	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		ProgramLibararyHolder holder = new ProgramLibararyHolder();
		ProgramLibararyModel dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.program_library_item,
					null);
			convertView.setTag(holder);
		}else {
			holder = (ProgramLibararyHolder) convertView.getTag();
		}
		holder.programcontainerpanel = (RelativeLayout)convertView.findViewById(R.id.programcontainerpanel);
		holder.line = (TextView)convertView.findViewById(R.id.txt_prog_line);
		
		if(dto.id.equals("1")){
			holder.programcontainerpanel.setVisibility(View.VISIBLE);
			holder.line.setVisibility(View.VISIBLE);
		}
		else{
			holder.programcontainerpanel.setVisibility(View.GONE);
			holder.line.setVisibility(View.GONE);
		}
		
		
		holder.program_img = (ImageView)convertView.findViewById(R.id.prog_img);
		holder.name = (TextView)convertView.findViewById(R.id.prog_name);
		holder.name.setText(dto.name);
		holder.programcontainerpanel.setTag(position+"");
		holder.programcontainerpanel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String position  =(String)arg0.getTag();
				ProgramLibararyModel dto = list.get(Integer.parseInt(position));
				String s = dto.url;
					if(dto.thumbnail.equalsIgnoreCase("program_1")){ //Special Noification
					DocumentFragment fragment = new DocumentFragment(s);
					FragmentManager fragmentManager = ((Activity)context).getFragmentManager();
					fragmentManager.beginTransaction()
					               .replace(R.id.content_frame, fragment)
					               .commit();
					}
					else if(dto.thumbnail.equalsIgnoreCase("program_2")){ // Case Study /Presentation
						ProgramDocumentFragment fragmen= new ProgramDocumentFragment(s);
						FragmentManager fragmentManage = ((Activity)context).getFragmentManager();
						fragmentManage.beginTransaction()
						               .replace(R.id.content_frame, fragmen)
						               .commit();
						
					}
					else if(dto.thumbnail.equalsIgnoreCase("program_3")){ // Program Document
						SpecialDocumentFragment fragmen= new SpecialDocumentFragment(s);
						FragmentManager fragmentManage = ((Activity)context).getFragmentManager();
						fragmentManage.beginTransaction()
						               .replace(R.id.content_frame, fragmen)
						               .commit();
						}
					else if(dto.thumbnail.equalsIgnoreCase("program_4")){ // Program Schudle
						SpecialDocumentFragment fragmen= new SpecialDocumentFragment(s);
						FragmentManager fragmentManage = ((Activity)context).getFragmentManager();
						fragmentManage.beginTransaction()
						               .replace(R.id.content_frame, fragmen)
						               .commit();
						}
					else{// Photo Gallery
						GalleryFragment fragmen= new GalleryFragment(dto);
						FragmentManager fragmentManage = ((Activity)context).getFragmentManager();
						fragmentManage.beginTransaction()
						               .replace(R.id.content_frame, fragmen)
						               .commit();
					}
				
				/*String position  =(String)arg0.getTag();
				 ProgramLibararyModel dto = list.get(Integer.parseInt(position));
				DocumentFragment fragment = new DocumentFragment(dto.url);
				FragmentManager fragmentManager = ((Activity)context).getFragmentManager();
				fragmentManager.beginTransaction()
				               .replace(R.id.content_frame, fragment)
				               .commit();*/
			}
		});
		if (dto.thumbnail != null) {
			holder.program_img.setImageResource(Utility.getImageResourceforIntrest(dto.thumbnail));
		}
		return convertView;

	}

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
class ProgramLibararyHolder {
	RelativeLayout programcontainerpanel;
	ImageView program_img;
	TextView name;
	TextView line;
}


