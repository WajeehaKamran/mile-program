package com.mile.adapters;

import java.util.List;

import com.mile.android.R;
import com.mile.activity.DownloadFileFromURL;
import com.mile.model.ProgramListModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

/** An array adapter that knows how to render views when given CustomData classes */
public class CustomArrayAdapter extends ArrayAdapter<ProgramListModel> {
    private LayoutInflater mInflater;
    public List<ProgramListModel> list;
    Context context;
    public CustomArrayAdapter(Context mcontext, List<ProgramListModel> values) {
        super(mcontext, R.layout.custom_data_view, values);
        mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        list = values;
        context = mcontext;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.custom_data_view, parent, false);

            // Create and save off the holder in the tag so we get quick access to inner fields
            // This must be done for performance reasons
            holder = new Holder();
            holder.textView = (TextView) convertView.findViewById(R.id.txt_doc_title);
            holder.file = (Button)convertView.findViewById(R.id.btn_file);
    		
    		
    		holder.file.setTag(position+"");
    		holder.file.setOnClickListener(new OnClickListener() {

    			@Override
    			public void onClick(View arg0) {
    				ProgramListModel programDoc = new ProgramListModel();
    				String position  =(String)arg0.getTag();
    				programDoc = list.get(Integer.parseInt(position));
    				String[] separated = programDoc.urlAdress.split("/");
    				String fileName = "";
    				fileName = separated[separated.length-1];
    				startDownload(programDoc.urlAdress,fileName.trim());
    			}
    		});

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ProgramListModel  dto = this.list.get(position);
        // Populate the text
        holder.textView.setText(dto.docTitle);

        // Set the color
       // convertView.setBackgroundColor(getItem(position).getBackgroundColor());

        return convertView;
    }
    private void startDownload(String fileURL,String fileName) {
	    new DownloadFileFromURL(context).execute(fileURL,fileName);
	}
    /** View holder for the views we need access to */
    private static class Holder {
        public TextView textView;
        Button file;
    }
}
