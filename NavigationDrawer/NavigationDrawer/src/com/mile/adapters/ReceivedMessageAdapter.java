package com.mile.adapters;
 


import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.activity.DownloadFileFromURL;
import com.mile.model.NotificationModel;
import com.mile.model.SpecialDocumentModel;
import com.mile.utility.ImageLoader;

public class ReceivedMessageAdapter extends BaseAdapter {
	public List<NotificationModel> list;
	protected Activity parentActivity;
	Context context;
	public ImageLoader imageLoader;
	public ReceivedMessageAdapter(Activity activity, List<NotificationModel> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		
		NotificationReceivedClassHolder holder = new NotificationReceivedClassHolder();
		NotificationModel  dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.received_message_item,
					null);
			convertView.setTag(holder);
		}else {
			holder = (NotificationReceivedClassHolder) convertView.getTag();
		}
		holder.messageTxt = (TextView)convertView.findViewById(R.id.txt_msg);
		holder.senderName = (TextView)convertView.findViewById(R.id.txt_msg_sender);		
		holder.messageTxt.setText(dto.message);
		holder.senderName.setText(dto.receiverName+","+"  " +dto.createDate);
		
		return convertView;

	}
	

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}

class NotificationReceivedClassHolder {
	TextView messageTxt;
	TextView senderName;
	TextView date;
}

