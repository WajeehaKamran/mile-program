package com.mile.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mile.android.R;
import com.mile.model.DocumentModel;
import com.mile.utility.ImageLoader;

public class AboutUSAdapter extends BaseAdapter {
	public List<String> list;
	protected Activity parentActivity;
	Context context;
	public ImageLoader imageLoader;
	public AboutUSAdapter(Activity activity, List<String> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}
	
	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		AboutUSHolder holder = new AboutUSHolder();
		String dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.about_us_item,
					null);
			convertView.setTag(holder);
		}else {
			holder = (AboutUSHolder) convertView.getTag();
		}
		
		holder.document_title =(TextView)convertView.findViewById(R.id.prog_name);
		holder.document_title.setText(dto);
		if (position == 0) {
			holder.document_title.setTextSize(20);
			holder.document_title.setGravity(Gravity.CENTER);
			holder.document_title.setTypeface(Typeface.DEFAULT_BOLD);
			holder.document_title.setVisibility(View.GONE);
			
			
		}
		
		else
		{
			holder.document_title.setVisibility(View.VISIBLE);
			holder.document_title.setTextSize(15);
			holder.document_title.setGravity(Gravity.LEFT);
			holder.document_title.setTypeface(Typeface.SERIF);
		}

		return convertView;

	}

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
class AboutUSHolder {
	
	TextView document_title;
}
