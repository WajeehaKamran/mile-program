package com.mile.adapters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.activity.ZoomImageAcitvity;
import com.mile.fragments.AboutMileFragment;
import com.mile.fragments.ImageWebFragment;
import com.mile.model.ProgramPhotoModel;
import com.mile.utility.Consts;
import com.mile.utility.ImageLoader;
import com.nostra13.universalimageloader.core.DisplayImageOptions;


public class GalleryAdapter extends ArrayAdapter<ProgramPhotoModel> {
	Context context;
	Activity mContext;
	List<ProgramPhotoModel> data = new ArrayList<ProgramPhotoModel>();
	ProgramPhotoModel dtoModel = new ProgramPhotoModel();
	//ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	View row ;
	DisplayImageOptions options, userImgOptions;
	private ProgressDialog pd;
	public ImageLoader imageLoader;
	public GalleryAdapter(Activity context, int layoutResourceId,
			List<ProgramPhotoModel> data) {
		super(context, layoutResourceId, data);
		this.context = context;
		this.mContext = context;
		this.data = data;
		imageLoader=new ImageLoader(context,150);

//		options = new DisplayImageOptions.Builder()
//				.showImageOnLoading(R.drawable.ic_stub)
//				.showImageForEmptyUri(R.drawable.ic_stub).cacheOnDisc(true)
//				.considerExifParams(true)
//				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
//				.displayer(new RoundedBitmapDisplayer(20)).build();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		row = convertView;
		RecordHolder holder = null;
		LayoutInflater inflater = ((Activity) context).getLayoutInflater() ;
			row = inflater.inflate(R.layout.gallery_row, parent, false);
			holder = new RecordHolder();
			holder.imageItem = (ImageView) row.findViewById(R.id.imgItem);
			holder.btnshare = (Button) row.findViewById(R.id.btn_share);
			holder.imgTitle = (TextView) row.findViewById(R.id.textView1);
			row.setTag(holder);
			holder.btnshare.setTag(holder);
			holder.imageItem.setTag(position+"");
			holder.imgTitle.setText(data.get(position).photoTitle);
			holder.btnshare.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					RecordHolder hold =new RecordHolder();
					hold = (RecordHolder)v.getTag();
					Uri bmpUri = getLocalBitmapUri(hold.imageItem);
					Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND); 
					sharingIntent.setType("text/plain");
					String shareBody = "";
					sharingIntent.setType("image/*");
					sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
					sharingIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
					context.startActivity(Intent.createChooser(sharingIntent, "Share With Friends"));			
					
					
				}
			});
		holder.imageItem.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String tag = (String)v.getTag();
				ProgramPhotoModel dtoProgram = new ProgramPhotoModel();
				dtoProgram = data.get(Integer.parseInt(tag));
				Intent intent = new Intent(context,
						ZoomImageAcitvity.class);
				intent.putExtra("imageURL", dtoProgram.photoURL);
				context.startActivity(intent); 
				
			}
		});

		final ProgramPhotoModel item = data.get(position);

		if (!Consts.GridScrollingState) {

			if (item.photoURL != null) {
				imageLoader.DisplayImage(item.thumbnail,holder.imageItem, true,0);
			}
		} else {
			holder.imageItem.setImageBitmap(null);
			holder.imageItem.setImageDrawable(null);
			holder.imageItem.setBackgroundResource(0);
		}

		

		return row;

	}

	static class RecordHolder {
		ImageView imageItem;
		Button btnshare;
		TextView imgTitle;
	}
	public Uri getLocalBitmapUri(ImageView imageView) {
		   
	    Drawable drawable = imageView.getDrawable();
	    Bitmap bmp = null;
	    if (drawable instanceof BitmapDrawable){
	       bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
	    } else {
	       return null;
	    }
	    
	    Uri bmpUri = null;
	    try {
	        File file =  new File(Environment.getExternalStoragePublicDirectory(  
	            Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
	        file.getParentFile().mkdirs();
	        FileOutputStream out = new FileOutputStream(file);
	        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
	        out.close();
	        bmpUri = Uri.fromFile(file);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return bmpUri;
	}
	/*
	 * Get Zipshot Detail
	 */
	
    
}
