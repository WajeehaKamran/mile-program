package com.mile.adapters;

import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.fragments.ProgramLibararyFragment;
import com.mile.model.ProgramModel;
import com.mile.utility.Consts;
import com.mile.utility.ImageLoader;

public class ProgramAdapter extends BaseAdapter {
	public List<ProgramModel> list;
	protected Activity parentActivity;
	Context context;
	public ImageLoader imageLoader;
    
	public ProgramAdapter(Activity activity, List<ProgramModel> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}
	
	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		ProgramHolder holder = new ProgramHolder();
		ProgramModel dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.program_item,
					null);
			convertView.setTag(holder);
		}else {
			holder = (ProgramHolder) convertView.getTag();
		}
		holder.programcontainerpanel = (RelativeLayout)convertView.findViewById(R.id.programcontainerpanel);
		holder.program_img = (ImageView)convertView.findViewById(R.id.prog_img);
		holder.name = (TextView)convertView.findViewById(R.id.prog_name);
		holder.name.setText(dto.name);
		holder.programcontainerpanel.setTag(position+"");
		holder.programcontainerpanel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String position  =(String)arg0.getTag();
				ProgramModel programModelDto = list.get(Integer.parseInt(position));
				Consts.CURRENT_PROGRAM = programModelDto;
				ProgramLibararyFragment fragment = new ProgramLibararyFragment(programModelDto);
				
				FragmentManager fragmentManager = ((Activity)context).getFragmentManager();
				fragmentManager.beginTransaction()
				               .replace(R.id.content_frame, fragment)
				               .commit();
			}
		});
		if (dto.smallimage != null) {
			imageLoader.DisplayImage(dto.smallimage, holder.program_img, true,0);
		}
		return convertView;

	}

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
class ProgramHolder {
	RelativeLayout programcontainerpanel;
	ImageView program_img;
	TextView name;
}


