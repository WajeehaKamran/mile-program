package com.mile.adapters;


import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.activity.DownloadFileFromURL;
import com.mile.model.ProgramDocument;
import com.mile.model.ProgramListModel;
import com.mile.model.SpecialDocumentModel;
import com.mile.utility.ImageLoader;

public class ProgramListAdapter extends BaseAdapter {
	public List<ProgramListModel> list;
	protected Activity parentActivity;
	Context context;
	public ImageLoader imageLoader;
	public ProgramListAdapter(Activity activity, List<ProgramListModel> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		
		ProgramListModelHolder holder = new ProgramListModelHolder();
		ProgramListModel  dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.special_document_fragment,
					null);
			convertView.setTag(holder);
		}else {
			holder = (ProgramListModelHolder) convertView.getTag();
		}
		holder.docname = (TextView)convertView.findViewById(R.id.txt_doc_title);
		holder.docname.setText(dto.docTitle);
		holder.file = (Button)convertView.findViewById(R.id.btn_file);
		
		
		holder.file.setTag(position+"");
		holder.file.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ProgramListModel programDoc = new ProgramListModel();
				String position  =(String)arg0.getTag();
				programDoc = list.get(Integer.parseInt(position));
				String[] separated = programDoc.urlAdress.split("/");
				String fileName = "";
				fileName = separated[separated.length-1];
				startDownload(programDoc.urlAdress,fileName.trim());
			}
		});

		return convertView;

	}
	private void startDownload(String fileURL,String fileName) {
	    new DownloadFileFromURL(context).execute(fileURL,fileName);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}

class ProgramListModelHolder {
	Button file;
	TextView docname;
}
