package com.mile.adapters;

import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.fragments.ProgramLibararyFragment;
import com.mile.model.ProgramModel;
import com.mile.model.UsersModel;
import com.mile.utility.Consts;
import com.mile.utility.ImageLoader;

public class UsersNotificationAdapter extends BaseAdapter {
	public List<UsersModel> list;
	protected Activity parentActivity;
	Context context;
	public ImageLoader imageLoader;
    
	public UsersNotificationAdapter(Activity activity, List<UsersModel> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}
	
	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		ProgramUserHolder holder = new ProgramUserHolder();
		UsersModel dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.prog_notification_item,
					null);
			convertView.setTag(holder);
		}else {
			holder = (ProgramUserHolder) convertView.getTag();
		}
  		holder.name = (TextView)convertView.findViewById(R.id.prog_name);
		holder.name.setText(dto.name);
		
		return convertView;

	}

	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
class ProgramUserHolder {
	TextView name;
}


