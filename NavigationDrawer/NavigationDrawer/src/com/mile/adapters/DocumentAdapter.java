package com.mile.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.model.DocumentModel;
import com.mile.utility.ImageLoader;

public class DocumentAdapter extends BaseAdapter {
	public List<DocumentModel> list;
	protected Activity parentActivity;
	Context context;
	public ImageLoader imageLoader;
	public DocumentAdapter(Activity activity, List<DocumentModel> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}
	
	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		DocumentHolder holder = new DocumentHolder();
		DocumentModel dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.document_item,
					null);
			convertView.setTag(holder);
		}else {
			holder = (DocumentHolder) convertView.getTag();
		}
		holder.programcontainerpanel = (RelativeLayout)convertView.findViewById(R.id.programcontainerpanel);
		holder.program_img = (ImageView)convertView.findViewById(R.id.prof_img);
		holder.name = (TextView)convertView.findViewById(R.id.name);
		holder.document = (TextView)convertView.findViewById(R.id.url_address);
		holder.name.setText(dto.name);
		holder.email =(TextView)convertView.findViewById(R.id.email);
		holder.email.setText(dto.emailAddress);
		holder.document_title =(TextView)convertView.findViewById(R.id.doc_title);
		holder.document.setText("");
		holder.email.setText(dto.emailAddress);
		if (dto.profilImage != null) {
			imageLoader.DisplayImage(dto.profilImage, holder.program_img, true,0);
		}
//		if (dto.smallimage != null) {
//			//			BaseFragment.imageLoader.displayImage("http://yellowzip.com/"
//			//					+ item1.thumbnail, holder.leftImg, options,
//			//					animateFirstListener);
//			imageLoader.DisplayImage("http://yellowzip.com/"
//					+ dto.thumbnail, holder.zsImg, true,0);
//		}
		return convertView;

	}

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
class DocumentHolder {
	RelativeLayout programcontainerpanel;
	ImageView program_img;
	TextView name;
	TextView email;
	TextView document;
	TextView document_title;
}
