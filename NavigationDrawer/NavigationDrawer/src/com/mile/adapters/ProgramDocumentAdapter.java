package com.mile.adapters;


import java.util.List;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mile.android.R;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.mile.activity.DownloadFileFromURL;
import com.mile.model.DocumentModel;
import com.mile.utility.ImageLoader;

public class ProgramDocumentAdapter extends BaseAdapter {
	public List<DocumentModel> list;
	protected Activity parentActivity;
	Activity context;
	public ImageLoader imageLoader;
	public ProgramDocumentAdapter(Activity activity, List<DocumentModel> list) {
		this.parentActivity = activity;
		context = activity;
		this.list = list;
		imageLoader=new ImageLoader(activity,150);
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {

		ProgramDocumentHolder holder = new ProgramDocumentHolder();
		DocumentModel  dto = this.list.get(position);
		if (convertView == null) { 
			LayoutInflater inflater = parentActivity.getLayoutInflater();
			convertView = inflater.inflate(R.layout.program_document_fragment,
					parent,false);
			convertView.setTag(holder);
		}else {
			holder = (ProgramDocumentHolder) convertView.getTag();
		}
		holder.mHlvCustomList = (HorizontalListView)convertView.findViewById(R.id.hlvCustomList);
		holder.designLayout = (RelativeLayout)convertView.findViewById(R.id.design_layout_ios);
		holder.docname = (TextView)convertView.findViewById(R.id.textView1);
		holder.profile_Image = (ImageView)convertView.findViewById(R.id.document_program_item_profile_image);
		holder.docname.setText(dto.name);

//		holder.m_listview = (GridView)convertView.findViewById(R.id.listView1);


//		ProgramListAdapter zipAdapter = new ProgramListAdapter(context,dto.documentList);
//		holder.m_listview.setAdapter(zipAdapter); 
		
		CustomProgramAdapter adapter = new CustomProgramAdapter(context, dto.documentList);

        // Assign adapter to HorizontalListView
		holder.mHlvCustomList.setAdapter(adapter);
//		holder.m_listview.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				int itemPosition     = position;
//				Toast.makeText(context.getApplicationContext(),
//						"Position :"+itemPosition+"  ListItem : " +"" , Toast.LENGTH_LONG)
//						.show();
//
//			}
//
//		}); 

		if (dto.profilImage != null) {
			imageLoader.DisplayImage(dto.profilImage, holder.profile_Image, true,0);
		}
		return convertView;

	}
	private void startDownload(String fileURL,String fileName) {
		new DownloadFileFromURL(context).execute(fileURL,fileName);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		
		return 0;
	}
}

class ProgramDocumentHolder {
	RelativeLayout designLayout;
	TextView docname,tv;
	ImageView profile_Image;
//	GridView m_listview;
	HorizontalListView mHlvCustomList;

}
