package com.mile.utility;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;

import com.mile.model.DocumentModel;
import com.mile.model.ProgramModel;
import com.mile.model.ProgramPhotoModel;


public class Consts {

	public static String currentLocale = "";

	// Server API Format
	public static final String API_FORMAT = "api_name={API_NAME}&format=json&json={JSON}";
	public static final String MULTIPART_API_FORMAT = "?api_name={API_NAME}&format=json";
	public static final String API_NAME_PARM = "{API_NAME}";
	public static final String API_JSON_PARM = "{JSON}";
	public static String CYRRENT_USER ="";
	public static String ANDROID_ID = "";
	public static String VERSION_NUMBER = "VERSION_NUMBER";
	public static String LOGIN_STATE_LOGOUT = "";
	public static String LOGIN_STATE_LOCAL = "1";
	public static String LOGIN_STATE_FACEBOOK = "2";
	public static String LOGIN_STATE_AVOIDED = "3";
	public static String ACCESS_TOKEN = "";
	public static int SPLASH_TIME_OUT =3000;
	

	public static final String imageName = "IMG.JPEG";
	public static final String SERVER_URL = "http://yellowzip.com/api/zipshots/";
	public static ProgramModel CURRENT_PROGRAM = new ProgramModel();
	//File uploading 
	public static  String fileName = "";
	public static  int progress = 0;
	// APIs
	public static final String LOG_IN = "http://yellowzip.com/api/login";
	public static final String FORGOT_PASSWORD_EMAIL = "http://yellowzip.com/api/users/forgotpassword";
	// http://yellowzip.com/api/facebookLogin
	public static final String FB_LOG_IN = "http://yellowzip.com/api/signup/";
	public static final String INSERT_ZIPSHOT = "http://yellowzip.com/api/zipshots/insert?";
	//http://yellowzip.com/api/zipshots/lisitng?
	//public static final String GET_ALL_ZIPSHOTS = "http://yellowzip.com/api/ZipshotsEvents/lisitng?";
	public static final String GET_ALL_PROGRAMS_LIBRARY ="http://www.api.mile.org/programs/";
	//public static final String GET_ALL_PROGRAMS_LIBRARY ="http://www.api.mile.org/programs";
	public static final String GET_ALL_PROGRAMS ="http://api.mile.org/programs";
	public static final String GET_ALL_MAINSPECIALDOCS ="http://www.api.mile.org/specialdocuments";
	public static final String GET_ALL_USERS ="http://www.api.mile.org/usersdevice/";
	public static final String GET_ALL_PROGRAMDOC ="http://www.api.mile.org/programs/42/programdocuments";
	public static final String GET_ALL_DOCUMENTS ="http://api.mile.org/programs/";
	public static final String GET_ABOUT_US ="http://www.api.mile.org/aboutus";
	public static final String GET_ALL_EVENTS = "http://yellowzip.com/api/events/listing/?";
	///api/events/listing/?area=77077&miles=20&interestid=0&type=local&offset=0&limit=50
	public static final String ZIPSHOT_DETAIL = "http://yellowzip.com/api/zipshots/detail/";
	public static final String ZIPSHOT_COMMENST = "http://yellowzip.com/api/zipshotComments/?zipshotid=";
	public static final String SEARCH_ZIPSHOT = "http://yellowzip.com/api/zipshots/";
	public static final String SEARCH_NEIGHBOURHOOD = "http://yellowzip.com/api/area/neighborhoods/?area=";
	public static final String PROGRAM_GALLERY = "http://www.api.mile.org/programs/42/photogallery";
	
	public static final String DELETE_ZIPSHOT = "http://yellowzip.com/api/zipshots/delete/";
	public static final String LIKE_ZIPSHOT = "http://yellowzip.com/api/zipshots/like/";
	public static final String LIKE_EVENTS = "http://yellowzip.com/api/events/like/";

	public static final String LIKE_ZIPCHATS= "http://yellowzip.com/api/Zipchats/like/";
//http://yellowzip.com/api/businessSignup
	public static final String SIGN_UP = "http://yellowzip.com/api/businessSignup?json=1";
	public static final String SIGN_UP_CONSUMER= "http://yellowzip.com/api/signup?json=1";
	//#define CMD_SIGNUP @"signup?json=1"

	public static final String ZIPSHOT_CHAT_POSTING = "http://yellowzip.com/api/zipchats/insert";
	
	// public static final String UPLOAD_IMAGE =
	// "http://yellowzip.com/api/zipshots/uploadImageStream/";
	public static final String UPLOAD_IMAGE = "http://yellowzip.com/api/zipshots/uploadImage";
	public static final String GET_INTERESTS = "http://yellowzip.com/api/interests/listing/";
	public static final String GET_INTERESTS_COUNT_LIST = "http://yellowzip.com/api/zipshots/interests/?area=";
	public static final String GET_CATGEORY_LIST = "http://yellowzip.com/api/zipchats/listing/?area=";
	public static final String GET_BUSINESS_LIST = "http://yellowzip.com/api/locations/detail/";
	public static final String GET_FILTERS = "http://yellowzip.com/api/zipshots/accessFilters/";
	public static final String GET_NEIGHBOUR = "http://yellowzip.com/api/Area/neighborhoods?area=";
	public static final String GET_BUSINESS_NAME = "http://yellowzip.com/api/locations/";
	public static final String ADD_COMMENT = "http://yellowzip.com/api/zipshots/addComment/";

	public static final String MESSAGE_LIST = "http://yellowzip.com/api/messages/lisitng?";
	public static final String INSERT_MESSAGE = "http://yellowzip.com/api/messages/insert?";
	
	public static final String GET_USER_PROFILE = "http://yellowzip.com/api/users/me?";
	public static final String UPDATE_PROFILE = "http://yellowzip.com/api/users/";
	public static final String UPDATE_FILTERS = "http://yellowzip.com/api/zipshots/updateAccessFilter?access_filter_type=";
	public static final String UPLOAD_PROFILE_IMAGE = "http://yellowzip.com/api/voiceImages/insert/?profileimage=1";
	
	public static final String REPORT_ABUSE = "http://yellowzip.com/api/zipshots/reportAbuse/?";
	public static List<ProgramModel> PROG_LIST = new ArrayList<ProgramModel>();

	
	
	public static HashMap<String, String> imageToUpload = new HashMap<String, String>();
	
	public static String postZipcode;
	public static String RECEIVERID = "";
	public static String MESSAGESTATUS = "";
	public static String postPlace;
	public static String postAddress;
	public static String postDesc;
	public static String postImageName;
	public static int galleryCount = 0;
	public static int postSelectedInterestPosition;
	public static Uri fileUri;
	public static Uri originalFileUri;
	public static Boolean isDiscardChanges = false;
	public static HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();
	public static HashMap<String, SoftReference<Bitmap>> imageCache = new HashMap<String, SoftReference<Bitmap>>();

	public static String POST_BUTTON_STATE = "default";
	public static Point ScreenSize;
	public static String selectedUserID = "";
	public static int successfullyPostingAPhoto = 0;
	public static int selectedEventInterest = 0;
	public static int selectedEventDistance = 40;
	public static String selectedCategoryID = "1036";
	public static String selectedBusinessID = "1036";
	public static String selectedBusinessCategoryName = "Choose Business Category";//"Automobile";
	public static String selectedBusinessName = "Knowledge Arts Foundation";
	public static String selectedInterestId = "";
	public static String selectedInterestTitle = "";
	public static int selectedInterestPhotoCount = 0;
	public static String selectedZipcode = "Houston TX 77077";
	public static String selectedPlace = "";
	public static int cornerRadiusZipshot = 0;
	public static int cornerRadiusUser = 90;
	public static  String neighbourhoodzipcode = "47010";
	public static Boolean isInterestListAvailable = false;
	public static String selectedFilter = "";
	public static Boolean isFilterListAvailable = false;
	public static Boolean isZipcodeListAvailable = false;
	public static Boolean isNeighbourListAvailable = false;
	public static Boolean isCityListAvailable = false;
	public static Boolean isZipcodeLoadingInProgress = false;
	public static Boolean isEventFilterValueChanged = false;
	public static Boolean isLoginSkip = false;
	public static String LBS_Status = "on";
	public static ProgramPhotoModel CURRENT_ZIPSHOT = new ProgramPhotoModel();
	
	public static String lastLoginState = LOGIN_STATE_LOGOUT; //0=logout, 1=local, 2=facebook

	public static int getSelectedEventInterest() {
		return selectedEventInterest;
	}

	public static void setSelectedEventInterest(int selectedEventInterest) {
		Consts.selectedEventInterest = selectedEventInterest;
	}

	public static int getSelectedEventDistance() {
		return selectedEventDistance;
	}

	public static void setSelectedEventDistance(int selectedEventDistance) {
		Consts.selectedEventDistance = selectedEventDistance;
	}

	public static Boolean getIsInterestListAvailable() {
		return isInterestListAvailable;
	}

	public static void setIsInterestListAvailable(
			Boolean isInterestListAvailable) {
		Consts.isInterestListAvailable = isInterestListAvailable;
	}

	

	public static String POST_CAMERA;

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}
	
	
	public static boolean AppStateScrolling = false;
	public static int startIndex = 0;
	public static int lastIndex = 20;
	public static boolean GridScrollingState = false;
	
	public static boolean isHomeFirstTime = false;
	
	public static boolean isGalleryFirstTime = false;
	
	public static List<ProgramPhotoModel> GALLERY_LIST = new ArrayList<ProgramPhotoModel>();
	public static List<DocumentModel>listDocuments = new  ArrayList<DocumentModel>();
	public static ProgramModel ProgramObj = new ProgramModel();
	
	public static UserModel Current_User = new UserModel();

	public static boolean isLogEnabled=true;
	
	public static boolean isFromCamera =  false;
	public static boolean isValidPost =  false;
	public static String PostValidityError =  "";
	
	

}
