package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mile.android.R;
import com.mile.adapters.ProgramAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.ProgramModel;
import com.mile.utility.AppPreferences;
import com.mile.utility.Consts;

public class ProgramFragment extends Fragment {
	public static final String ARG_MER_NUMBER = "planet_number";
	TextView txt;
	ZipshotBL zsBL;
	private ProgressDialog pd;
	Button back;
	TextView welcumUser;
	AppPreferences prefs;
	ProgramAdapter zipAdapter;
	List<ProgramModel>listPrograms = new  ArrayList<ProgramModel>();
	ListView zipsListView;
	public ProgramFragment() {
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prog_list, container, false);
        zsBL = new ZipshotBL(getActivity());
        prefs = new AppPreferences();
        return rootView;
    }
    @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
    	txt =  (TextView)view.findViewById(R.id.prog_name);
    	welcumUser = (TextView) view.findViewById(R.id.welcome_user);
    	welcumUser.setText("Welcome " + Consts.CYRRENT_USER+"!" );
    	zipsListView = (ListView) view.findViewById(R.id.programs_list_view);
    	back = (Button) view.findViewById(R.id.back_btn);
    	back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new MainMenuFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
    	new GetInterestsTask().execute();
    }
    class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllPrograms();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				zipAdapter = new ProgramAdapter(getActivity(), listPrograms);
				zipsListView.setAdapter(zipAdapter);

			}
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}
    private String getUserNameString(){
		String userName = prefs.getUserValue(getActivity()).toLowerCase();
		return userName.substring(0,1).toUpperCase()+ userName.substring(1,userName.length()).toLowerCase();
	}
}
