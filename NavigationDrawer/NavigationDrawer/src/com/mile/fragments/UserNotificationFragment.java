package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;


import com.mile.android.R;
import com.mile.adapters.ProgramNotificationAdapter;
import com.mile.adapters.UsersNotificationAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.fragments.ProgramNotificationFragment.GetInterestsTask;
import com.mile.fragments.ProgramNotificationFragment.ProgNotificationTask;
import com.mile.model.ProgramModel;
import com.mile.model.UsersModel;
import com.mile.utility.AppPreferences;
import com.mile.utility.Consts;
import com.mile.utility.UserModel;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class UserNotificationFragment extends Fragment {
	Spinner progSpin;
	private ProgressDialog pd;
	EditText msg;
	TextView welcumTxt;
	ZipshotBL zsBL;
	String progid="";
	Button postmsg;
	Button back;
	RelativeLayout tab1,tab2,tab3;
	int spinnerPosition = 0;
	UsersNotificationAdapter adapter;
	ProgramModel prog;
	String notificationResponse;
	List<UsersModel>listPrograms = new  ArrayList<UsersModel>();
	UserModel model;
	String mSelected = "";
	ProgNotificationTask mWorkerTask;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.notification_user_fragment, container, false);
		zsBL = new ZipshotBL(getActivity());
		mWorkerTask = new ProgNotificationTask();
		return rootView;
	}
	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		progSpin = (Spinner) view.findViewById(R.id.spinner_prog_notif);
		welcumTxt = (TextView)view.findViewById(R.id.welcum_txt);
		welcumTxt.setText("Welcome " + Consts.CYRRENT_USER+"!" );
		tab1 = (RelativeLayout)view.findViewById(R.id.tab1);
		tab1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		tab2 = (RelativeLayout)view.findViewById(R.id.tab2);
		tab2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new UserNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		tab3 = (RelativeLayout)view.findViewById(R.id.tab3);
		tab3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new SendItemsFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		progSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				UsersModel  dto = listPrograms.get(position);
				mSelected = dto.id;

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {


			}

		});
		msg = (EditText)view.findViewById(R.id.editText2);
		postmsg = (Button)view.findViewById(R.id.button1);
		back = (Button)view.findViewById(R.id.back_btn);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Fragment mFragment = new MainMenuFragment(); 
				getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, mFragment ).commit(); 

			}
		});
		new GetInterestsTask().execute();
		postmsg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(!msg.getText().toString().isEmpty()){
				mWorkerTask.execute();
				}
				else{
					Toast.makeText(getActivity().getApplicationContext(), "Please enter text to send", 
							   Toast.LENGTH_LONG).show();
				}


			}
		});


	}
	void setSpinnerAdapter(){



	}
	class ProgNotificationTask extends AsyncTask<Void, Void, Void> {



		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd  = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			zsBL = new ZipshotBL(getActivity());
			String msg01 = msg.getText().toString();
			notificationResponse = zsBL.SingleUserInAPI(Consts.Current_User.id,mSelected,msg01);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pd != null) {
				pd.dismiss();
			}

			if (Consts.MESSAGESTATUS.equalsIgnoreCase("success")){
				msg.setText("");
				Toast.makeText(getActivity(), "Message sent successfully", Toast.LENGTH_LONG).show();
				Fragment mFragment = new MainMenuFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
			
			}else
			{
				Toast.makeText(getActivity(), "Message sending failed.Please try again", Toast.LENGTH_LONG).show();
			}
		}
	}

	class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();


		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllUsers();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				adapter = new UsersNotificationAdapter(getActivity(), listPrograms);
				progSpin.setAdapter(adapter);

			}
			//setSpinnerAdapter();
			if (pd != null) {
				pd.dismiss();
			}

		}
	}



}

