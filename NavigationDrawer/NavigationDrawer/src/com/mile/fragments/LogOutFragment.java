package com.mile.fragments;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.mile.android.R;
import com.mile.activity.LoginScreen;
import com.mile.bl.ZipshotBL;
import com.mile.utility.Consts;

public class LogOutFragment extends Fragment {
	public static final String ARG_LOG_OUT_NUMBER = "LogOutFragment";
	public static final String REG_ID = "regId";
	public static final String EMAIL_ID = "eMailId";
	Button btnLogOut;
	public LogOutFragment() {
	       
    }
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.logout_fragment, container, false);
        
				onRemoveAccountDetails();
				Intent login = new Intent(getActivity(), LoginScreen.class);
				login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(login);
				getActivity().finish();
				

        return view;
    }
	private void onRemoveAccountDetails() {
		
		FileOutputStream fOut = null;
		Consts.lastLoginState = "";
		OutputStreamWriter osw = null;

		try{
			SharedPreferences prefs = getActivity().getSharedPreferences("UserDetails",
					Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(REG_ID, "");
			editor.putString(EMAIL_ID, "");
			editor.commit();
			fOut = getActivity().openFileOutput("loginstate.dat", Context.MODE_PRIVATE);

			osw = new OutputStreamWriter(fOut);

			osw.write(Consts.lastLoginState);

			osw.close();

			fOut.close();

		}catch(Exception e){

			e.printStackTrace(System.err);

		}

		String user = "";
		String pass = "";

		fOut = null;

		osw = null;

		try{

			fOut = getActivity().openFileOutput("userData.dat", Context.MODE_PRIVATE);

			osw = new OutputStreamWriter(fOut);

			osw.write(user+";"+pass);

			osw.close();

			fOut.close();

		}catch(Exception e){

			e.printStackTrace(System.err);

		}
	}

   
}
