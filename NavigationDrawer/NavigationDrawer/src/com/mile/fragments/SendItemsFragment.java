package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import com.mile.android.R;
import com.mile.adapters.NotificationAdapter;
import com.mile.adapters.ReceivedMessageAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.fragments.NotificationFragment.GetInterestsTask;
import com.mile.model.NotificationModel;
import com.mile.utility.Consts;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SendItemsFragment extends Fragment {
	RelativeLayout tab1,tab2,tab3;
	Button back;
	ZipshotBL zsBL;
	private ProgressDialog pd;
	String url = "http://www.api.mile.org/users/";
	ReceivedMessageAdapter zipAdapter;
	List<NotificationModel>listPrograms = new  ArrayList<NotificationModel>();
	TextView welcumTxt;
	ListView zipsListView;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.send_notifi_fragment, container, false);
        zsBL = new ZipshotBL(getActivity());
         return rootView;
    }
   @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		welcumTxt = (TextView)view.findViewById(R.id.welcum_txt);
		welcumTxt.setText("Welcome " + Consts.CYRRENT_USER+"!" );
		zipsListView = (ListView) view.findViewById(R.id.listView1);
		back = (Button)view.findViewById(R.id.back_btn);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Fragment mFragment = new MainMenuFragment(); 
				getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, mFragment ).commit(); 

			}
		});
		tab1 = (RelativeLayout)view.findViewById(R.id.tab1);
		tab1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		tab2 = (RelativeLayout)view.findViewById(R.id.tab2);
		tab2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new UserNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		tab3 = (RelativeLayout)view.findViewById(R.id.tab3);
		tab3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new SendItemsFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		new GetInterestsTask().execute();
}
   class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllMessagesReceivedUser(url);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				zipAdapter = new ReceivedMessageAdapter(getActivity(),listPrograms);
				zipsListView.setAdapter(zipAdapter);

			}
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}
}
