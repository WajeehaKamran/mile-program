package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mile.android.R;
import com.mile.adapters.AboutUSAdapter;
import com.mile.adapters.DocumentAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.AboutUSModel;
import com.mile.model.DocumentModel;
import com.mile.utility.Consts;

public class AboutMileFragment extends Fragment {
	public static final String ARG_ABOUT_NUMBER = "AboutMileFragment";
	ZipshotBL zsBL;
	private ProgressDialog pd;
	TextView about;
	Button back;
	AboutUSModel aboutUSObject = new AboutUSModel();
	AboutUSAdapter docAdapter;
	ListView list;
	List<String>listobj = new  ArrayList<String>();
	public AboutMileFragment() {
	       
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_about_screen, container, false);
        zsBL = new ZipshotBL(getActivity());
        return rootView;
    }
   @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		back = (Button)view.findViewById(R.id.back_btn);
		//
		list = (ListView) view.findViewById(R.id.listView1);
		new GetDocumentTask().execute();
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new MainMenuFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
}
   class GetDocumentTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			aboutUSObject = zsBL.getAboutUS();
			listobj.add(aboutUSObject.title);
			listobj.add(aboutUSObject.text01);
			listobj.add(aboutUSObject.text02);
			listobj.add(aboutUSObject.text03);
			

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		
				docAdapter = new AboutUSAdapter(getActivity(), listobj);
				list.setAdapter(docAdapter);

			
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}
}
