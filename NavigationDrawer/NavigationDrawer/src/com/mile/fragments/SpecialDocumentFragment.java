package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mile.android.R;
import com.mile.adapters.SpecialDocumentsAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.SpecialDocumentModel;
import com.mile.utility.Consts;

public class SpecialDocumentFragment extends Fragment {
	public static final String ARG_PRODOC_NUMBER = "PRODOC_number";
	TextView txt,welcumTxt;
	ZipshotBL zsBL;
	private ProgressDialog pd;
	Button back;
	String url;
	SpecialDocumentsAdapter zipAdapter;
	List<SpecialDocumentModel>listPrograms = new  ArrayList<SpecialDocumentModel>();
	ListView zipsListView;
	public SpecialDocumentFragment(String s) {
        this.url = s;
    }
	public SpecialDocumentFragment() {
       
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pro_document, container, false);
        zsBL = new ZipshotBL(getActivity());
        return rootView;
    }
    @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
    	txt =  (TextView)view.findViewById(R.id.prog_name);
    	zipsListView = (ListView) view.findViewById(R.id.programs_document_list_view);
    	welcumTxt = (TextView)view.findViewById(R.id.welcum_txt);
    	welcumTxt.setText("Welcome " + Consts.CYRRENT_USER+"!" );
    	back = (Button) view.findViewById(R.id.back_btn);
    	back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramLibararyFragment(Consts.CURRENT_PROGRAM); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
    	new GetInterestsTask().execute();
    }
    class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllSpecialdProgramDoc(url);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				zipAdapter = new SpecialDocumentsAdapter(getActivity(),listPrograms);
				zipsListView.setAdapter(zipAdapter);

			}
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}
}