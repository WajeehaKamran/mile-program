package com.mile.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mile.android.R;

public class ImageWebFragment extends Fragment {
	public static final String ARG_IMAGE_NUMBER = "ImageWebFragment";
	String imageStr;
	public ImageWebFragment(String ImageStr) {
		imageStr = ImageStr;
	       
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity__property_web, container, false);
        WebView mWebView = (WebView)view.findViewById(R.id.containWebView);
		mWebView.loadUrl(imageStr);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setSaveFormData(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.setWebViewClient(new MyWebViewClient());
        return view;
    }
   @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
	}

	 class MyWebViewClient extends WebViewClient
		{
			@Override
			//show the web page in webview but not in web browser
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl (url);
				return true;
			}
		}

	}


