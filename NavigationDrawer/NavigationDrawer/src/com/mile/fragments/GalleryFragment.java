package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import com.mile.android.R;
import com.mile.adapters.GalleryAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.ProgramLibararyModel;
import com.mile.model.ProgramPhotoModel;
import com.mile.utility.Consts;
import com.mile.utility.ImageLoader;

public class GalleryFragment extends Fragment {
	public static final String ARG_GEL_NUMBER = "planet_number";
	String galleryUrl;
	ProgramLibararyModel dto;
	public String toString() {
		return "GalleryFragment";
	}
	public GalleryFragment(ProgramLibararyModel s) {
		this.dto = s;
	}
	
	Button back,btnMore;
	
	TextView welcomettx;
	private ProgressDialog pd;
	GridView gridView;
	List<ProgramPhotoModel> gridList = new ArrayList<ProgramPhotoModel>();
	GalleryAdapter galleryAdapter;

	//FragmentChangeListener fc;
	ZipshotBL zsBL;
	public ImageLoader imageLoader;
	TextView txtNoPostMessage;
	public GalleryFragment() {
        
    }
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.fragment_gallery, container,
				false);

		//fc = (FragmentChangeListener) getActivity();
		zsBL = new ZipshotBL(getActivity());
		imageLoader=new ImageLoader(getActivity(),150);
		Consts.lastIndex = 5;
		

		return rootView;
	}
	
	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		txtNoPostMessage = (TextView) view.findViewById(R.id.noYellowZip);
		gridView = (GridView) view.findViewById(R.id.galleryGrid);
		welcomettx = (TextView)view.findViewById(R.id.welcum_txt);
		welcomettx.setText("Welcome " + Consts.CYRRENT_USER+"!" );
		back = (Button) view.findViewById(R.id.back_button);
		btnMore = (Button) view.findViewById(R.id.btn_more);
		
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramLibararyFragment(Consts.CURRENT_PROGRAM); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		btnMore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new GetUserZipshotsTask().execute();
			}
		});
		new GetUserZipshotsTask().execute();
			/*if (Consts.isGalleryFirstTime) {
				new GetUserZipshotsTask().execute();
				Consts.isGalleryFirstTime = false;
			} else {
				if(Consts.GALLERY_LIST.size() > 0)
				{
				galleryAdapter = new GalleryAdapter(getActivity(), 0,
						Consts.GALLERY_LIST);
				gridView.setAdapter(galleryAdapter);
				galleryAdapter.notifyDataSetChanged();
				if(pd!=null){
					pd.dismiss();
				}
				}
				else {
					txtNoPostMessage.setVisibility(View.VISIBLE);
				}
			}*/
		

		gridView.setFastScrollEnabled(false);
		gridView.setSmoothScrollbarEnabled(true);
		gridView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView absListView,
					int scrollState) {
				if (scrollState == GridView.OnScrollListener.SCROLL_STATE_IDLE) {
					Consts.GridScrollingState = false;
					galleryAdapter.notifyDataSetChanged();

				} else {

					Consts.GridScrollingState = true;
				}

			}

			@Override
			public void onScroll(AbsListView absListView, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

			}
		});

	}

	 
	class GetUserZipshotsTask extends AsyncTask<Void, Void, Void> {
	List<ProgramPhotoModel> gLIST = new ArrayList<ProgramPhotoModel>();
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			galleryUrl = dto.url+"/"+Consts.startIndex+"/"+Consts.lastIndex+"/limitstart";
			
				Consts.GALLERY_LIST = zsBL.getUserZipshots(galleryUrl);
			

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (pd != null) {
				pd.dismiss();
			}
			
			System.out.println("ZipHome List Size ========= "
					+ Consts.GALLERY_LIST.size());

			
			if (Consts.GALLERY_LIST.size() > 0) {
			
				Consts.lastIndex += 5;
				txtNoPostMessage.setVisibility(View.GONE);

				
				galleryAdapter = new GalleryAdapter(getActivity(), 0,
						Consts.GALLERY_LIST);
				gridView.setAdapter(galleryAdapter);

			}else{
				txtNoPostMessage.setVisibility(View.VISIBLE);
			}

			

		}
	}


}