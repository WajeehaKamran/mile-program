package com.mile.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.mile.android.R;
import com.mile.bl.ZipshotBL;

public class SendNotificationFragment extends Fragment {
	ZipshotBL zsBL;
	RelativeLayout tab1,tab2;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.send_notifi_fragment, container, false);
        zsBL = new ZipshotBL(getActivity());
        return rootView;
    }
   @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		tab1 = (RelativeLayout)view.findViewById(R.id.tab1);
		tab1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		tab2 = (RelativeLayout)view.findViewById(R.id.tab1);
		tab2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new UserNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
}
}
