package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mile.android.R;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.mile.adapters.ProgramDocumentAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.DocumentModel;

import com.mile.model.ProgramDocument;
import com.mile.model.ProgramModel;
import com.mile.utility.Consts;
import com.mile.utility.Utility;

public class ProgramDocumentFragment extends Fragment {
	public static final String ARG_PRODOC_NUMBER = "PRODOC_number";
	TextView txt,welcumTxt;
	Button back;
	ZipshotBL zsBL;
	private ProgressDialog pd;
	String url;
	ProgramDocumentAdapter zipAdapter;
	List<DocumentModel>listPrograms = new  ArrayList<DocumentModel>();
	ListView zipsListView;
	
	public ProgramDocumentFragment(String s) {
        this.url =s;
    }
	public ProgramDocumentFragment() {
       
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pro_document, container, false);
        zsBL = new ZipshotBL(getActivity());
        return rootView;
    }
    @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState)  {
    	txt =  (TextView)view.findViewById(R.id.prog_name);
    	welcumTxt = (TextView)view.findViewById(R.id.welcum_txt);
    	welcumTxt.setText("Welcome " + Consts.CYRRENT_USER+"!" );
    	zipsListView = (ListView) view.findViewById(R.id.programs_document_list_view);
    	Utility.setListViewHeightBasedOnChildren(zipsListView);
    	back = (Button)view.findViewById(R.id.back_btn);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramLibararyFragment(Consts.CURRENT_PROGRAM); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
    	new GetInterestsTask().execute();
    }
    class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllDocuments(url);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				zipAdapter = new ProgramDocumentAdapter(getActivity(), listPrograms);
				zipsListView.setAdapter(zipAdapter);
				
		          
			}
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}
}