package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.adapters.DocumentAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.DocumentModel;
import com.mile.utility.Consts;
import com.mile.utility.ImageLoader;

public class DocumentFragment extends Fragment {
    public static final String ARG_PLANET_NUMBER = "planet_number";
	ZipshotBL zsBL;
	Button back;
	DocumentAdapter docAdapter;
	private ProgressDialog pd;
	TextView txtTitle;
	ListView list;
	String documentUrl;
	public ImageLoader imageLoader;
	List<DocumentModel>listDocuments = new  ArrayList<DocumentModel>();

    public DocumentFragment(String id) {
       documentUrl = id;
    }
   
    public DocumentFragment() {
        
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_program, container, false);
        zsBL = new ZipshotBL(getActivity());
     
        return rootView;
    }
    @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		list = (ListView) view.findViewById(R.id.list);
		txtTitle = (TextView)view.findViewById(R.id.textView1);
		back = (Button)view.findViewById(R.id.back_btn);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramLibararyFragment(Consts.CURRENT_PROGRAM); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		new GetDocumentTask().execute();
    }
    class GetDocumentTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listDocuments = zsBL.getAllDocuments(documentUrl);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listDocuments.size() > 0) {

				docAdapter = new DocumentAdapter(getActivity(), listDocuments);
				list.setAdapter(docAdapter);

			}
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}
    
}