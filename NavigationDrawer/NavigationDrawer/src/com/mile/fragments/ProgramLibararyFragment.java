package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.mile.android.R;
import com.mile.adapters.ProgramLibrarayAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.ProgramLibararyModel;
import com.mile.model.ProgramModel;
import com.mile.utility.Consts;

public class ProgramLibararyFragment extends Fragment {
	public static final String ARG_MER_NUMBER = "ProgramLibararyFragment";
	TextView txt;
	ZipshotBL zsBL;
	private ProgressDialog pd;
	Button back;
	TextView welcumtxt;
	ProgramLibrarayAdapter zipAdapter;
	ProgramModel programModelObject;
	List<ProgramLibararyModel>listPrograms = new  ArrayList<ProgramLibararyModel>();
	ListView zipsListView;

	public ProgramLibararyFragment(ProgramModel programDto) {
		this.programModelObject = programDto;
	} 
	
	public ProgramLibararyFragment() {

	} 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_prog_librray, container, false);
		zsBL = new ZipshotBL(getActivity());
		return rootView;
	}
	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		txt =  (TextView)view.findViewById(R.id.prog_name);
		welcumtxt = (TextView) view.findViewById(R.id.welcome_txt);
		welcumtxt.setText("Welcome " + Consts.CYRRENT_USER+"!" );
		zipsListView = (ListView) view.findViewById(R.id.programs_list_view);
		back = (Button)view.findViewById(R.id.back_btn);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		ProgramModel dto = new ProgramModel();
		//public String d =dto.id;
		new GetInterestsTask().execute();
		if (listPrograms.size() > 0) {

			zipAdapter = new ProgramLibrarayAdapter(getActivity(), listPrograms);
			zipsListView.setAdapter(zipAdapter);

		}
	}

	class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllProgramsLibarary(programModelObject);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				zipAdapter = new ProgramLibrarayAdapter(getActivity(), listPrograms);
				zipsListView.setAdapter(zipAdapter);

			}
			if (pd != null) {
				pd.dismiss();
			}

		}
	}
}
