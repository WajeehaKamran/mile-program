package com.mile.fragments;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import com.mile.android.R;
import com.mile.activity.LoginScreen;
import com.mile.utility.AppPreferences;
import com.mile.utility.Consts;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainMenuFragment extends Fragment {
	public static final String ARG_MAIN_NUMBER = "planet_number";
	AppPreferences prefs;
	public MainMenuFragment() {
	       
    }
	RelativeLayout btnProg,btnNot,btnDoc,btnAbtMile,btnSendNot,btnlogout;
	TextView welcomeuser;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_menu_fragment, container, false);
        prefs = new AppPreferences(); 
        return rootView;
    }
   @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		welcomeuser = (TextView) view.findViewById(R.id.txt_welcome);
		welcomeuser.setText("Welcome " + Consts.CYRRENT_USER+"!" );
		btnProg = (RelativeLayout)view.findViewById(R.id.ll_btn_program);
		btnAbtMile= (RelativeLayout)view.findViewById(R.id.ll_btn_about_mile);
		btnNot = (RelativeLayout)view.findViewById(R.id.ll_btn_notification);
		btnSendNot = (RelativeLayout)view.findViewById(R.id.relativeLayout2);
		btnlogout = (RelativeLayout)view.findViewById(R.id.containerlogout);
		btnDoc = (RelativeLayout)view.findViewById(R.id.ll_btn_special_documents);
		if(Consts.Current_User.is_admin.equalsIgnoreCase("1"))
		
		{
			btnSendNot.setVisibility(View.VISIBLE);
		}
		else
		{
			btnSendNot.setVisibility(View.GONE);
		}
		
		btnSendNot.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		btnDoc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new MainSpecialDocumentFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		btnNot.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new NotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		btnAbtMile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new AboutMileFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		btnProg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		btnlogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onRemoveAccountDetails();
				Intent login = new Intent(getActivity(), LoginScreen.class);
				login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(login);
				getActivity().finish();
				

			}

			private void onRemoveAccountDetails() {
				prefs.getRemoveUserValues(getActivity());
				Consts.lastLoginState = "";
				Consts.ACCESS_TOKEN = "";
				Consts.CYRRENT_USER = "";
				Consts.isGalleryFirstTime = true;
				FileOutputStream fOut = null;

				OutputStreamWriter osw = null;

				try{

					fOut = getActivity().openFileOutput("loginstate.dat", Context.MODE_PRIVATE);

					osw = new OutputStreamWriter(fOut);

					osw.write(Consts.lastLoginState);

					osw.close();

					fOut.close();

				}catch(Exception e){

					e.printStackTrace(System.err);

				}

				String user = "";
				String pass = "";

				fOut = null;

				osw = null;

				try{

					fOut = getActivity().openFileOutput("userData.dat", Context.MODE_PRIVATE);

					osw = new OutputStreamWriter(fOut);

					osw.write(user+";"+pass);

					osw.close();

					fOut.close();

				}catch(Exception e){

					e.printStackTrace(System.err);

				}
			}
		});
   }
   
}
