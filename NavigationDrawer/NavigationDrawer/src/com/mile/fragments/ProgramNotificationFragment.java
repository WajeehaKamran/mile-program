package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;
import com.mile.android.R;
import com.mile.activity.LoginScreen;
import com.mile.adapters.ProgramAdapter;
import com.mile.adapters.ProgramNotificationAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.model.ProgramModel;
import com.mile.utility.AppPreferences;
import com.mile.utility.Consts;
import com.mile.utility.UserModel;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class ProgramNotificationFragment extends Fragment {
	Spinner progSpin;
	public static final String ARG_PRONOT_NUMBER = "planet_number";
	private ProgressDialog pd;
	EditText msg;
	ZipshotBL zsBL;
	TextView welcumTxt;
	String progid="";
	Button postmsg;
	Button back;
	int spinnerPosition = 0;
	RelativeLayout tab1,tab2,tab3;
	ProgramNotificationAdapter adapter;
	ProgramModel prog;
	String notificationResponse;
	List<ProgramModel>listInterst = new  ArrayList<ProgramModel>();
	List<ProgramModel>listPrograms = new  ArrayList<ProgramModel>();
	UserModel model;
	String mSelected = "";
	Context mContext = getActivity();

	public ProgramNotificationFragment() {
        
    }
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notification_postprogram_fragment, container, false);
        zsBL = new ZipshotBL(getActivity());
         return rootView;
    }
   @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		welcumTxt = (TextView)view.findViewById(R.id.welcum_txt);
		welcumTxt.setText("Welcome " + Consts.CYRRENT_USER+"!" );
		tab3 = (RelativeLayout)view.findViewById(R.id.tab3);
		tab3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new SendItemsFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		tab1 = (RelativeLayout)view.findViewById(R.id.tab1);
		tab1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new ProgramNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		tab2 = (RelativeLayout)view.findViewById(R.id.tab2);
		tab2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new UserNotificationFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		progSpin = (Spinner) view.findViewById(R.id.spinner_prog_notif);
		progSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				ProgramModel  dto = listPrograms.get(position);
				mSelected = dto.id;
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
	
				
			}
			
		});
		msg = (EditText)view.findViewById(R.id.prog_msg);
		postmsg = (Button)view.findViewById(R.id.btn_postmsg);
		back = (Button)view.findViewById(R.id.back_btn);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new MainMenuFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
		new GetInterestsTask().execute();
		postmsg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(!msg.getText().toString().isEmpty()){
				new ProgNotificationTask().execute();
				}
				else{
					Toast.makeText(getActivity().getApplicationContext(), "Please enter text to send", 
							   Toast.LENGTH_LONG).show();
				}
				
			}
		});
		
   }
   void setSpinnerAdapter(){
		


	}
   class ProgNotificationTask extends AsyncTask<Void, Void, Void> {

		
	   
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			
			
		    notificationResponse = zsBL.UserMeassageInAPI(Consts.Current_User.id,mSelected,msg.getText().toString().trim());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(pd!=null)
			{
				pd.dismiss();
			}
			if (Consts.MESSAGESTATUS.equalsIgnoreCase("success")){
					
				Toast.makeText(getActivity().getApplicationContext(), "Message sent sucessfully", 
						   Toast.LENGTH_LONG).show();
				Fragment mFragment = new MainMenuFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit();
				}else
				{
					Toast.makeText(getActivity().getApplicationContext(), "Message sending failed", 
							   Toast.LENGTH_LONG).show();
				}
		}
   }
	
   class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllPrograms();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				adapter = new ProgramNotificationAdapter(getActivity(), listPrograms);
				progSpin.setAdapter(adapter);

			}
			//setSpinnerAdapter();
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}


 
   }

