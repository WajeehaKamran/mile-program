package com.mile.fragments;

import java.util.ArrayList;
import java.util.List;

import com.mile.android.R;
import com.mile.adapters.NotificationAdapter;
import com.mile.adapters.SpecialDocumentsAdapter;
import com.mile.bl.ZipshotBL;
import com.mile.fragments.SpecialDocumentFragment.GetInterestsTask;
import com.mile.model.NotificationModel;
import com.mile.model.SpecialDocumentModel;
import com.mile.utility.Consts;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
 
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class NotificationFragment extends Fragment {
	public static final String ARG_NOTIF_NUMBER = "NotificationFragment";
	TextView txt,welcumTxt;
	ZipshotBL zsBL;
	private ProgressDialog pd;
	Button back;
	String url = "http://www.api.mile.org/users/";
	NotificationAdapter zipAdapter;
	List<NotificationModel>listPrograms = new  ArrayList<NotificationModel>();
	ListView zipsListView;
	public NotificationFragment() {
	       
    }
	Button btnProNot,btnUserNot;
	 
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notification_fragment, container, false);
        zsBL = new ZipshotBL(getActivity());
        return rootView;
    }
   @Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		welcumTxt = (TextView)view.findViewById(R.id.welcum_txt);
    	welcumTxt.setText("Welcome " + Consts.CYRRENT_USER+"!" );
	 	zipsListView = (ListView) view.findViewById(R.id.listView1);
    	back = (Button) view.findViewById(R.id.button1);
    	back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment mFragment = new MainMenuFragment(); 
				 getFragmentManager().beginTransaction()
	            .replace(R.id.content_frame, mFragment ).commit(); 
				
			}
		});
    	new GetInterestsTask().execute();
    }
    class GetInterestsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null) {
				pd.dismiss();
			}
			pd = new ProgressDialog(getActivity());
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			listPrograms = zsBL.getAllMessagesByUser(url);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (listPrograms.size() > 0) {

				zipAdapter = new NotificationAdapter(getActivity(),listPrograms);
				zipsListView.setAdapter(zipAdapter);

			}
			if (pd != null) {
				pd.dismiss();
			}
			
		}
	}

}
