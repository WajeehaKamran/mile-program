package com.mile.bl;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.mile.servercom.ServerCom;
import com.mile.utility.Consts;
import com.mile.utility.UserModel;


public class ProfileBL {

	Activity act;
	Context ctx;
	ServerCom server;

	public ProfileBL(final Activity act) {
		this.act = act;
		ctx = act.getApplicationContext();
		server = new ServerCom();
	}

	public UserModel getUserProfile(String accessToken) {

		JSONObject response;
		UserModel userObj = null;

		try {

			String url = Consts.GET_USER_PROFILE + "&token=" + accessToken;
			Log.e("Profile URl", url);

			response = new ServerCom().getZipshotDetailJSON(ctx, url);
			Log.e("Profile Response", response.toString());

			if (response != null) {

				String message = response.optString("message");
				if(message.contains("Permission")){
					userObj = null;
				}else{
					userObj = parseUser(response);
					System.out.println("User Id ======== " + userObj.id);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return userObj;

	}

	// Update Profile
	public String updateUserProfile(String id ,String firstName, String zipCode,
			String email, String newPassowrd, String voiceId, boolean isChangePassword) {

		String response = "";

		String serverMessage = "";
		try {

			String url = Consts.UPDATE_PROFILE +id+ "/?token="
					+ Consts.ACCESS_TOKEN;

			response = server.postJSONObject(
					url,
					updateProfileRequest(firstName, zipCode, email, newPassowrd, voiceId, isChangePassword));

			if (response != null && !(response.equals(""))) {

				JSONObject jsonResJsonObject = new JSONObject(response);

				if (jsonResJsonObject != null) {
					System.out.println("Update Profile Response ===== "
							+ jsonResJsonObject.optString("message"));
					String usrid  = jsonResJsonObject.optString("id");
					if(usrid.length()> 0 )

					serverMessage = "Updated successfully" ;
					else
						serverMessage = jsonResJsonObject.optString("message") ;	

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return serverMessage;

	}
	//Update User filters
	public String updateUserFilters(String id, String status) {

		JSONObject response = null;
		String serverMessage = "";
		try {

			String url = Consts.UPDATE_FILTERS +id + "&status="+status + "&token="
					+ Consts.ACCESS_TOKEN+ "&json=1";
			response = new ServerCom().getJSONResult(ctx, url);
			if (response != null) {
				for (int i = 0; i < response.length(); i++) {
					String code = response.getString("message");
						if(code.equalsIgnoreCase("OK"))
						{
							serverMessage = "Success";
						}
						
					}
	
				}
	
			} catch (Exception e) {
				e.printStackTrace();
			}

		return serverMessage;

	}
	private JSONObject updateProfileRequest(String firstName, String zipCode,
			String email, String newPassowrd,
			String voiceId, boolean isPasswordChange) {
		JSONObject json = new JSONObject();

		try {
			json.put("full_name", firstName);
			json.put("zipcode", zipCode);
			json.put("email", email);
			if (isPasswordChange) {
				
				json.put("password", newPassowrd);
				json.put("confirm_password", newPassowrd);
			}
			json.put("id", voiceId);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	private UserModel parseUser(JSONObject json) {
		UserModel user = new UserModel();

		user.id = json.optString("id");
		user.first_name = json.optString("first_name");
		user.last_name = json.optString("last_name");
		user.email = json.optString("email");
		

		return user;
	}

}
