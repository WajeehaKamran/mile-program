package com.mile.bl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.mile.model.AboutUSModel;
import com.mile.model.DocumentModel;
import com.mile.model.NotificationModel;
import com.mile.model.ProgramLibararyModel;
import com.mile.model.ProgramListModel;
import com.mile.model.ProgramModel;
import com.mile.model.ProgramPhotoModel;
import com.mile.model.SpecialDocumentModel;
import com.mile.model.UsersModel;
import com.mile.servercom.ServerCom;
import com.mile.utility.AppPreferences;
import com.mile.utility.Consts;
import com.mile.utility.UserModel;



public class ZipshotBL {

	Activity act;
	Context ctx;
	ServerCom server;
	AppPreferences prefs;

	public ZipshotBL(final Activity act) {
		this.act = act;
		ctx = act.getApplicationContext();
		server = new ServerCom();
		prefs = new AppPreferences();
	}

	// login
	public String logInAPI(String email, String password) {
		String strResponse = "";
		UserModel current_user = new UserModel();
		try {

			String url = "http://api.mile.org/users/login";
			//			Utility.logToFile(ctx, "In-LoginAPI:: URL :: LOGIN :: " + url, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: USER :: " + email, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: PASS :: " + password, "Message");
			strResponse = server
					.createHttpPost(url,"username",email,"password",password);
			//			Utility.logToFile(ctx, "In-LoginAPI:: SERVER RESPONSE :: " + strResponse, "Message");
			if (strResponse != null && strResponse.length() > 0) {

				JSONObject jsonObj = new JSONObject(strResponse);



				String status = jsonObj.optString("status");


				if(status.equalsIgnoreCase("success")){
					strResponse = "success";

					current_user.id = jsonObj.optString("userid");
					current_user.first_name = jsonObj.optString("username");
					String[] temp = current_user.first_name.split("@");
					Consts.CYRRENT_USER = temp[0];
					current_user.email = jsonObj.optString("username");

					current_user.is_admin = jsonObj.optString("isAdmin");
					Consts.Current_User = current_user;

				}

				else
				{
					strResponse = "failure";
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			//			Utility.logToFile(ctx, "In-LoginAPI:: EXCEPTION :: " + e.getMessage(), "ERROR");
		}

		return strResponse;

	}

	public String registerDeviceIdAPI(String email, String regID) {
		String strResponse = "";
		UserModel guest = new UserModel();
		try {

			String url = "http://api.mile.org/users/registertoken";
			//			Utility.logToFile(ctx, "In-LoginAPI:: URL :: LOGIN :: " + url, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: USER :: " + email, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: PASS :: " + password, "Message");
			strResponse = server
					.createHttpPost(url,"username",email,"deviceToken",regID);
			Log.e("Registration ID", regID);
			//			Utility.logToFile(ctx, "In-LoginAPI:: SERVER RESPONSE :: " + strResponse, "Message");
			if (strResponse != null && strResponse.length() > 0) {

				JSONObject jsonObj = new JSONObject(strResponse);
				String user = jsonObj.optString("username");
				//Consts.CYRRENT_USER = user;
				String status = jsonObj.optString("status");


				if(status.equalsIgnoreCase("success"))
					strResponse = "success";
				else
				{
					strResponse = "failure";
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			//			Utility.logToFile(ctx, "In-LoginAPI:: EXCEPTION :: " + e.getMessage(), "ERROR");
		}

		return strResponse;

	}
	public String UserMeassageInAPI(String senderid, String receiverid, String message) {
		String strResponse = "";
		try {

			String url = "http://api.mile.org/sendmessagetoprogramparticipants";
			//			Utility.logToFile(ctx, "In-LoginAPI:: URL :: LOGIN :: " + url, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: USER :: " + email, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: PASS :: " + password, "Message");
			strResponse = server
					.createHttpPost(url,"senderUserID",senderid,"programID",receiverid,"message",message);
			//			Utility.logToFile(ctx, "In-LoginAPI:: SERVER RESPONSE :: " + strResponse, "Message");
			if (strResponse != null && strResponse.length() > 0) {

				JSONObject jsonObj = new JSONObject(strResponse);



				String status = jsonObj.optString("status");


				if(status.equalsIgnoreCase("success")){
					strResponse = "success";
					String msg = jsonObj.optString("msg");
					Consts.MESSAGESTATUS = jsonObj.optString("status");


				}

				else
				{
					strResponse = "failure";
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return strResponse;

	}
	public String SingleUserInAPI(String senderid, String receiverid, String message) {
		String strResponse = "";
		try {

			String url = "http://api.mile.org/singleusermessage";
			//			Utility.logToFile(ctx, "In-LoginAPI:: URL :: LOGIN :: " + url, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: USER :: " + email, "Message");
			//			Utility.logToFile(ctx, "In-LoginAPI:: PASS :: " + password, "Message");
			strResponse = server
					.createHttpPost(url,"senderUserID",senderid,"reciverUserID",receiverid,"message",message);
			//			Utility.logToFile(ctx, "In-LoginAPI:: SERVER RESPONSE :: " + strResponse, "Message");
			if (strResponse != null && strResponse.length() > 0) {

				JSONObject jsonObj = new JSONObject(strResponse);



				String status = jsonObj.optString("status");


				if(status.equalsIgnoreCase("success")){
					strResponse = "success";
					String msg = jsonObj.optString("msg");
					Consts.MESSAGESTATUS = jsonObj.optString("status");


				}

				else
				{
					strResponse = "failure";
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return strResponse;

	}
	public String forgotPasswordAPI(String email) {

		String strResponse = "";
		try {

			String url = Consts.FORGOT_PASSWORD_EMAIL;
			strResponse = server
					.postJSONObject(url, jsonForgotPassword(email));
			if (strResponse != null && strResponse.length() > 0) {

				JSONObject jsonObj = new JSONObject(strResponse);
				strResponse = jsonObj.getString("message");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return strResponse;

	}
	public List<SpecialDocumentModel> getAllMainSpecialDocs() {

		JSONArray response;
		List<SpecialDocumentModel> list = new ArrayList<SpecialDocumentModel>();


		SpecialDocumentModel mainZip;

		try {
			String url  = Consts.GET_ALL_MAINSPECIALDOCS ;

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new SpecialDocumentModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleSpecialDocs(j1);


					list.add(mainZip);

					// System.out.println("Final object list 1 ====== "
					// + mainZip.object1.description);
					// System.out.println("2nd list object desc ====== "
					// + mainZip.object2.description);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// for (int j = list.size() - 1; j >= 0; j--) {
		//
		// tempZip = list.get(j);
		// tempList.add(tempZip);
		// }

		return list;

	}

	public List<ProgramModel> getAllPrograms() {

		JSONArray response;
		List<ProgramModel> list = new ArrayList<ProgramModel>();


		ProgramModel mainZip;

		try {
			String url  = Consts.GET_ALL_PROGRAMS ;

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new ProgramModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleProgram(j1);


					list.add(mainZip);

					// System.out.println("Final object list 1 ====== "
					// + mainZip.object1.description);
					// System.out.println("2nd list object desc ====== "
					// + mainZip.object2.description);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// for (int j = list.size() - 1; j >= 0; j--) {
		//
		// tempZip = list.get(j);
		// tempList.add(tempZip);
		// }

		return list;

	}

	public List<UsersModel> getAllUsers() {

		JSONArray response;
		List<UsersModel> list = new ArrayList<UsersModel>();


		UsersModel mainZip;

		try {
			String url  = Consts.GET_ALL_USERS ;

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new UsersModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleUser(j1);


					list.add(mainZip);


				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// for (int j = list.size() - 1; j >= 0; j--) {
		//
		// tempZip = list.get(j);
		// tempList.add(tempZip);
		// }

		return list;

	}

	public List<SpecialDocumentModel> getAllSpecialdProgramDoc(String URL) {

		JSONArray response;
		List<SpecialDocumentModel> list = new ArrayList<SpecialDocumentModel>();


		SpecialDocumentModel mainZip;

		try {
			String url  = URL ;

			response = new ServerCom().getGsonArray(ctx, URL);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new SpecialDocumentModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleProgramDoc(j1);


					list.add(mainZip);

					// System.out.println("Final object list 1 ====== "
					// + mainZip.object1.description);
					// System.out.println("2nd list object desc ====== "
					// + mainZip.object2.description);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// for (int j = list.size() - 1; j >= 0; j--) {
		//
		// tempZip = list.get(j);
		// tempList.add(tempZip);
		// }

		return list;

	}
	public List<NotificationModel> getAllMessagesByUser(String URL) {

		JSONArray response;
		List<NotificationModel> list = new ArrayList<NotificationModel>();


		NotificationModel mainZip;

		try {
			String url  = URL + Consts.Current_User.id +"/messages" ;

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new NotificationModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleMessage(j1);


					list.add(mainZip);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;

	}
	public List<NotificationModel> getAllMessagesReceivedUser(String URL) {

		JSONArray response;
		List<NotificationModel> list = new ArrayList<NotificationModel>();


		NotificationModel mainZip;

		try {
			String url  = URL + Consts.Current_User.id +"/sentmessages" ;

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new NotificationModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleMessage(j1);


					list.add(mainZip);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;

	}
	public List<ProgramLibararyModel> getAllProgramsLibarary(ProgramModel dto) {

		JSONArray response;
		List<ProgramLibararyModel> list = new ArrayList<ProgramLibararyModel>();

		try {
			String url  = Consts.GET_ALL_PROGRAMS_LIBRARY+dto.id+"/main-menu" ;

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) {

				JSONObject json = (JSONObject) response.get(0);

				JSONArray notificationArr = json.getJSONArray("notification");
				JSONObject notificationJson = notificationArr.getJSONObject(0);
				ProgramLibararyModel program = new ProgramLibararyModel();
				program.id = notificationJson.optString("spcialNotification");
				program.url = notificationJson.optString("spcialNotificationURL");
				program.name = "Spcial Notifications";
				program.thumbnail = "program_1";
				list.add(program);

				JSONArray caseStudyArr = json.getJSONArray("caseStudyPresenationNotice");
				JSONObject caseStudyJson = caseStudyArr.getJSONObject(0);
				program = new ProgramLibararyModel();
				program.id = caseStudyJson.optString("caseStudyPresenation");
				program.url = caseStudyJson.optString("caseStudyPresenationURL");
				program.name = "Case Study/ Presentation";
				program.thumbnail = "program_2";
				list.add(program);

				JSONArray progrsmDocArr = json.getJSONArray("programDocumentNotice");
				JSONObject progrsmDocArrJson = progrsmDocArr.getJSONObject(0);
				program = new ProgramLibararyModel();
				program.id = progrsmDocArrJson.optString("programDocument");
				program.url = progrsmDocArrJson.optString("programDocumentURL");
				program.name = "Program Document";
				program.thumbnail = "program_3";
				list.add(program);

				JSONArray programScheduleArr = json.getJSONArray("programScheduleNotice");
				JSONObject programScheduleJson = programScheduleArr.getJSONObject(0);
				program = new ProgramLibararyModel();
				program.id = programScheduleJson.optString("programSchedule");
				program.url = programScheduleJson.optString("programScheduleURL");
				program.name = "Program Schedule";
				program.thumbnail = "program_4";
				list.add(program);

				JSONArray photosGalleryNoticeArr = json.getJSONArray("photosGalleryNotice");
				JSONObject photosGalleryNoticeJson = photosGalleryNoticeArr.getJSONObject(0);
				program = new ProgramLibararyModel();
				program.id = photosGalleryNoticeJson.optString("photosGallery");
				program.url = photosGalleryNoticeJson.optString("photosGalleryURL");
				program.name = "Photos Gallery";
				program.thumbnail = "program_5";
				list.add(program);

				JSONArray dailyPhotosGalleryNoticeArr = json.getJSONArray("dailyPhotosGalleryNotice");
				JSONObject dailyPhotosGalleryNoticeJson = dailyPhotosGalleryNoticeArr.getJSONObject(0);
				program = new ProgramLibararyModel();
				program.id = dailyPhotosGalleryNoticeJson.optString("dailyPhotosGallery");
				program.url = dailyPhotosGalleryNoticeJson.optString("dailyPhotosGalleryURL");
				program.name = "Daily Photos Gallery";
				program.thumbnail = "program_6";
				list.add(program);



			}



		} catch (Exception e) {
			e.printStackTrace();
		}


		return list;

	}

	public List<DocumentModel> getAllDocuments(String documentUrl) {


		JSONArray response;
		List<DocumentModel> list = new ArrayList<DocumentModel>();


		DocumentModel mainZip;

		try {

			response = new ServerCom().getGsonArray(ctx, documentUrl);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new DocumentModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleDocument(j1);


					list.add(mainZip);

					// System.out.println("Final object list 1 ====== "
					// + mainZip.object1.description);
					// System.out.println("2nd list object desc ====== "
					// + mainZip.object2.description);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// for (int j = list.size() - 1; j >= 0; j--) {
		//
		// tempZip = list.get(j);
		// tempList.add(tempZip);
		// }

		return list;

	}
	public AboutUSModel getAboutUS() {


		JSONArray response;
		AboutUSModel mainZip = null;

		try {

			response = new ServerCom().getGsonArray(ctx, Consts.GET_ABOUT_US);

			if (response != null) {
				mainZip = new AboutUSModel();
				JSONObject json = (JSONObject) response.get(0);
				mainZip.title = json.optString("mainTitle");
				mainZip.text01 = json.optString("text1");
				mainZip.text02 = json.optString("text2");
				mainZip.text03 = json.optString("text3");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}


		return mainZip;

	}

	public List<ProgramModel> searchZipShot(String keyWord) {

		JSONArray response; 
		List<ProgramModel> list = new ArrayList<ProgramModel>();


		ProgramModel mainZip;


		try {

			String url = Consts.SEARCH_ZIPSHOT + "keywords=" + keyWord + "/is_mobile=1";

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) { 

				for (int i = 0; i < response.length(); i++) {

					mainZip = new ProgramModel();

					if (i >= response.length())
						break;

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSingleProgram(j1);


					list.add(mainZip);

					// System.out.println("Final object list 1 ====== "
					// + mainZip.object1.description);
					// System.out.println("2nd list object desc ====== "
					// + mainZip.object2.description);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}


		return list;

	}

	/*
	 * Get user zipshots
	 */
	public List<ProgramPhotoModel> getUserZipshots(String galleryurl) {

		JSONArray response;
		List<ProgramPhotoModel> list = new ArrayList<ProgramPhotoModel>();

		ProgramPhotoModel mainZip;

		try {
			//
			String url = galleryurl;

			response = new ServerCom().getGsonArray(ctx, url);

			if (response != null) {

				for (int i = 0; i < response.length(); i++) {

					mainZip = new ProgramPhotoModel();

					JSONObject j1 = (JSONObject) response.get(i);

					mainZip = parseSinglePhotoProgram(j1);

					list.add(mainZip);

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;

	}



	/*
	 * Getting all interests
	 */
	//	public ArrayList<InterestModel> GetAllInterests() {
	//
	//		if(Consts.inretestList.isEmpty())
	//		{
	//
	//			JSONArray response;
	//			Consts.inretestList = new ArrayList<InterestModel>();
	//
	//			InterestModel obj;
	//
	//			try {
	//
	//				String url = Consts.GET_INTERESTS ;
	//
	//				response = new ServerCom().getGsonArray(ctx, url);
	//				obj = new InterestModel();
	//				if (response != null) {
	//
	//					obj = new InterestModel();
	//					obj.id = "0";
	//					obj.name = "Select Interest";
	//					Consts.inretestList.add(obj);
	//					for (int i = 0; i < response.length(); i++) {
	//
	//						obj = new InterestModel();
	//						JSONObject j = (JSONObject) response.get(i);
	//						obj = parseInterest(j);
	//						Consts.inretestList.add(obj);
	//					}
	//
	//				}
	//
	//			} catch (Exception e) {
	//				e.printStackTrace();
	//			}
	//		}
	//
	//		return Consts.inretestList;
	//
	//	}
	//	public ArrayList<InterestModel> GetAllInterestsPhotos() {
	//
	//
	//		JSONArray response;
	//		ArrayList<InterestModel> interestList = new ArrayList<InterestModel>();
	//
	//		InterestModel obj,obj1;
	//
	//		try {
	//
	//			String url = Consts.GET_INTERESTS_COUNT_LIST + Consts.selectedZipcode ;
	//
	//			response = new ServerCom().getGsonArray(ctx, url);
	//			obj = new InterestModel();
	//			if (response != null) {
	//
	//				obj = new InterestModel();
	//				
	//				obj.id = "0";
	//				obj.name = "Choose Interest";
	//				interestList.add(obj);
	//				
	//				obj = new InterestModel();
	//				obj.id = "1";
	//				obj.name = "All";
	//				interestList.add(obj);
	//				for (int i = 1; i <= response.length(); i++) {
	//
	//					obj1 = new InterestModel();
	//					JSONObject j = (JSONObject) response.get(i-1);
	//
	//					obj1.id = j.optString("id");
	//					obj1.name = j.optString("name");
	//					obj1.count = Integer.parseInt(j.optString("zipshot_count"));
	//					interestList.add(obj1);
	//				}
	//
	//			}
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//
	//		return interestList;
	//
	//	}
	//	public ArrayList<CategoryDTO> GetAllCategoriesForText(String categID, String zipcode) {
	//
	//
	//		JSONArray response;
	//		ArrayList<CategoryDTO> interestList = new ArrayList<CategoryDTO>();
	//
	//		CategoryDTO obj;
	//
	//		try {
	//
	//			String url = Consts.GET_CATGEORY_LIST+zipcode + "/offset=0" + "/catid=" + categID + "/limit=50" ;
	//
	//			response = new ServerCom().getGsonArray(ctx, url);
	//			obj = new CategoryDTO();
	//			if (response != null) {
	//
	//
	//				for (int i = 0; i < response.length(); i++) {
	//
	//					obj = new CategoryDTO();
	//					JSONObject j = (JSONObject) response.get(i);
	//
	//					obj.categoryName = j.optString("catname");
	//					obj.categoryID = j.optString("catid");
	//					obj.title = j.optString("title");
	//					obj.message = j.optString("message");
	//					obj.dateAdded = j.optString("dateadded");
	//					obj.neighbour_hood = j.optString("nb_hood");
	//					
	//					JSONObject user = j.getJSONObject("user");
	//					obj.userid = user.optString("id");
	//					obj.fullname = user.optString("full_name");
	//					obj.profileimg = user.optString("profile_image");
	//					
	//					obj.zipcode = j.optString("zipcode");
	//					obj.city =j.optString("city");
	//					obj.state = j.optString("state");
	//					interestList.add(obj);
	//				}
	//
	//			}
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//
	//		return interestList;
	//
	//	}
	//
	//	
	//	public BusinessDTO GetAllBusinessDetailsForBusinessID(String categID) {
	//
	//
	//		JSONObject obj;
	//		BusinessDTO dto = new BusinessDTO();
	//	
	//
	//		try {
	//
	//			String url = Consts.GET_BUSINESS_LIST+categID ;
	//
	//			obj = new ServerCom().getJSONResult(ctx, url);
	//			
	//			if (obj != null) {
	//				dto.business_name = obj.optString("name");
	//				dto.business_id = obj.optString("company_id");
	//				dto.email = obj.optString("email");
	//				dto.zipcode = obj.optString("zipcode");
	//				dto.city = obj.optString("city");
	//				dto.state = obj.optString("state");
	//				dto.city_state_zip = obj.optString("city_state_zip");
	//				dto.full_address = obj.optString("full_address");
	//				dto.phone = obj.optString("phone");
	//				dto.description = obj.optString("description");
	//				dto.url = obj.optString("url");
	//				dto.logo = obj.optString("logo");
	//				dto.website = obj.optString("website");
	//				JSONObject user = obj.getJSONObject("user");
	//				dto.userid = user.optString("id");
	//				dto.fullname = user.optString("full_name");
	//				dto.profileimg = user.optString("profile_image");
	//				
	//				
	//				
	//
	//			}
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//
	//		return dto ;
	//
	//	}
	//	public ArrayList<FiltersModel> GetAllFilter() {
	//		Consts.selectedFilter = "";
	//		JSONArray response;
	//		ArrayList<FiltersModel> filtersList = new ArrayList<FiltersModel>();
	//		FiltersModel obj;
	//
	//		try {
	//
	//			String url = Consts.GET_FILTERS+ "/?token="
	//					+ Consts.ACCESS_TOKEN +"&json=1";
	//
	//			response = new ServerCom().getGsonArray(ctx, url);
	//			//				obj = new FiltersModel();
	//			//				obj.id = "0";
	//			//				obj.title ="Select Filters";
	//			//				obj.current ="0";
	//			//				Consts.filtersList.add(obj);
	//			if (response != null) {
	//
	//				int fSelected = 0;
	//				for (int i = 0; i < response.length(); i++) {
	//
	//					obj = new FiltersModel();
	//					JSONObject j = (JSONObject) response.get(i);
	//
	//					obj = parseFilters(j);
	//					filtersList.add(obj);
	//
	//				}
	//				String[] array = new String[6];
	//				for (int i = 0; i < filtersList.size(); i++) {
	//
	//					if(filtersList.get(i).current.equalsIgnoreCase("1"))
	//					{
	//						if(fSelected == 0 )
	//						{
	//							array[fSelected] = filtersList.get(i).title;
	//
	//						}
	//						else
	//						{
	//							array[fSelected] = ", "+filtersList.get(i).title;
	//						}
	//						Consts.selectedFilter += array[fSelected];
	//						fSelected++;
	//					}
	//				}
	//
	//			}
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//
	//
	//		return filtersList;
	//
	//	}
	public ProgramPhotoModel zipShotDetail(String id) {

		JSONObject response;
		ProgramPhotoModel zipObject = new ProgramPhotoModel();

		try {

			String url = Consts.ZIPSHOT_DETAIL + id + "&json=1";

			response = new ServerCom().getZipshotDetailJSON(ctx, url);

			if (response != null) {

				//zipObject = parseSingleZipshot(response);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return zipObject;

	}

	public String GetBusinessName(String location_id) {

		JSONObject response;

		String business_name = null;
		try {

			String url = Consts.GET_BUSINESS_NAME+location_id+ "/?token="+ Consts.ACCESS_TOKEN;
			response = new ServerCom().getZipshotDetailJSON(ctx, url);

			if (response != null) {

				business_name = response.optString("name");


			}

		} catch (Exception e) {
			e.printStackTrace();
		}


		return business_name;

	}

	/*
	 * Getting all interests
	 */
	//	public ArrayList<NeighbourDTO> GetAllNeighbours(String area) {
	//
	//		//		if(Consts.neighboursList.isEmpty())
	//		//		{
	//
	//		JSONArray response;
	//		Consts.neighboursList = new ArrayList<NeighbourDTO>();
	//
	//		NeighbourDTO obj;
	//
	//		try {
	//
	//			String url = Consts.GET_NEIGHBOUR+area;
	//
	//			response = new ServerCom().getGsonArray(ctx, url);
	//			obj = new NeighbourDTO();
	//
	//			if (response != null) {
	//
	//
	//				for (int i = 0; i < response.length(); i++) {
	//
	//					obj = new NeighbourDTO();
	//					JSONObject j = (JSONObject) response.get(i);
	//
	//					obj = parseNeighbour(j);
	//					Consts.neighboursList.add(obj);
	//				}
	//
	//			}
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//		//}
	//
	//		return Consts.neighboursList;
	//
	//	}

	// add comment
	public String addComment(String id, String comment, String token) {

		String response="";

		try {

			String url = Consts.ADD_COMMENT + id + "&token=" + token
					+ "&json=1";
			response = server.postJSONObject(url,
					insertCommentRequest(id, comment));

			if (response != null) {

				System.out.println("Add Comment Response ===== "
						+ response.toString());

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

		return response;

	}

	// Report Abuse
	public String reportAbuse(String zipShotId, String description, String token) {

		String response = "";
		String temp = "";

		try {

			String url = Consts.REPORT_ABUSE + "&token=" + token + "&json=1";
			temp = server.postJSONObject(url,
					abuseRequest(zipShotId, description));

			if (temp != null) {

				if (temp.equals("1")) {
					response = temp;
				} else {
					JSONObject jsonResJsonObject = new JSONObject(temp);

					if (jsonResJsonObject != null) {
						response = jsonResJsonObject.optString("message");
					}
				}

				System.out.println("Add Comment Response ===== "
						+ response.toString());

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}
	private ProgramLibararyModel parseSingleProgramLibrarayItem(JSONObject json) {

		ProgramLibararyModel program = new ProgramLibararyModel();
		program.id = json.optString("id");
		program.name = json.optString("name");
		program.thumbnail = json.optString("smallImage");


		return program;
	}

	private SpecialDocumentModel parseSingleProgramDoc(JSONObject json) {

		SpecialDocumentModel program = new SpecialDocumentModel();
		program.urlAdress = json.optString("urlAdress");
		program.docTitle = json.optString("docTitle");
		return program;
	}
	private NotificationModel parseSingleMessage(JSONObject json) {

		NotificationModel program = new NotificationModel();
		program.id = json.optString("id");
		program.message = json.optString("message");
		program.createDate = json.optString("createdDate");
		program.senderName = json.optString("senderName");

		program.receiverName = json.optString("receiverName");
		
		return program;
	}

	private ProgramModel parseSingleProgram(JSONObject json) {

		ProgramModel program = new ProgramModel();
		program.id = json.optString("id");
		Consts.RECEIVERID = program.id;
		program.name = json.optString("name");
		program.short_url = json.optString("shortURL");
		program.status = json.optString("status");
		program.smallimage = json.optString("smallImage");
		program.bigimage = json.optString("bigImage");
		program.programManagerEmail = json.optString("programManagerEmail");
		program.start_date = json.optString("startDate");
		program.end_date = json.optString("endDate");


		return program;
	}
	private SpecialDocumentModel parseSingleSpecialDocs(JSONObject json) {

		SpecialDocumentModel program = new SpecialDocumentModel();
		program.docTitle = json.optString("docTitle");
		program.urlAdress = json.optString("urlAdress");
		return program;
	}

	private UsersModel parseSingleUser(JSONObject json) {

		UsersModel user = new UsersModel();
		user.id = json.optString("id");
		user.name = json.optString("name");
		return user;
	}
	private ProgramPhotoModel parseSinglePhotoProgram(JSONObject json) {

		ProgramPhotoModel program = new ProgramPhotoModel();
		program.id = json.optString("id");
		program.alumniId = json.optString("alumniId");
		program.photoTitle = json.optString("photoTitle");
		program.photoURL = json.optString("photoURL");
		program.photoURL = json.optString("photoURL");
		program.thumbnail = json.optString("thumbImage");

		return program;
	}


	private DocumentModel parseSingleDocument(JSONObject json) {
		ArrayList<ProgramListModel> commentList = new ArrayList<ProgramListModel>();
		ProgramListModel progObj;
		DocumentModel document = new DocumentModel();
		try{
			document.id = json.optString("id");
			document.name = json.optString("name");
			document.emailAddress = json.optString("emailAddress");
			document.docTitle = json.optString("docTitle");
			document.userId = json.optString("userid");
			document.profilImage = json.optString("profileImage");

			JSONArray arr = json.getJSONArray("documentList");

			for (int i = 0; i < arr.length(); i++) {
				JSONObject commentJson = arr.getJSONObject(i);
				progObj = new ProgramListModel();

				progObj.docTitle = commentJson.optString("docTitle");
				progObj.urlAdress = commentJson.optString("urlAdress");


				commentList.add(progObj);
			}
		}catch (JSONException e) {
			e.printStackTrace();

		}
		document.documentList = commentList;

		return document;
	}
	//	private EventsModel parseSingleEvent(JSONObject json) {
	//
	//		EventsModel zipShot = new EventsModel();
	//
	//		ArrayList<PlaceDTO> locationsList = new ArrayList<PlaceDTO>();
	//		PlaceDTO locationObj;
	//		try {
	//			zipShot.type = json.optString("type");
	//			zipShot.id = json.optString("id");
	//			zipShot.title = json.optString("title");
	//			zipShot.subject = json.optString("subject");
	//			zipShot.likes = json.optInt("likes");
	//			zipShot.description = json.optString("description");
	//			zipShot.thumbnail = json.optString("thumbnail");
	//			zipShot.website = json.optString("website");
	//			zipShot.interest_name = json.optString("interest_name");
	//			zipShot.interest_id = json.optInt("interestid");
	//			zipShot.start_date = json.optString("start_date_formatted");
	//			zipShot.end_date = json.optString("end_date_formatted");
	//			zipShot.start_time = json.optString("start_time");
	//			zipShot.end_time = json.optString("end_time");
	//			zipShot.start_weekday = json.optString("start_week_day");
	//			zipShot.end_weekday = json.optString("end_week_day");
	//			zipShot.timings = json.optString("timing");
	//			zipShot.phone_number = json.optString("phone");
	//			zipShot.zipcode = json.optString("zipcode");
	//			zipShot.address = json.optString("address");
	//			zipShot.place = json.optString("place");
	//			zipShot.display = json.optString("display");
	//			zipShot.days_date = json.optString("days_date");
	//			zipShot.image = json.optString("image");
	//			zipShot.user_fav = json.optString("user_favorited");
	//			zipShot.city_state_zipcode = json.optString("city_state_zipcode");
	//			
	//
	//
	//			JSONObject userJson = json.getJSONObject("user");
	//
	//
	//			zipShot.user_name  = userJson.optString("full_name");
	//			zipShot.user_image  = userJson.optString("profile_image");
	//			zipShot.user_id  = userJson.optString("id");
	//			zipShot.posted_by = userJson.optString("first_name");
	//			
	//
	//			zipShot.url = json.optString("url");
	//			
	//			zipShot.location_id = json.optInt("locationid");
	//			
	//			if(zipShot.location_id != 0 )
	//			{
	//
	//				JSONObject locationJson = json.getJSONObject("location");
	//
	//				locationObj = new PlaceDTO();
	//
	//				locationObj.name = locationJson.optString("name");
	//				locationObj.city_state_zipcode = locationJson.optString("city_state_zipcode");
	//				
	//				locationObj.url = locationJson.optString("url");
	//				locationsList.add(locationObj);      
	//
	//			} 
	//			
	//
	//
	//		}catch (JSONException e) {
	//			e.printStackTrace();
	//
	//		}
	//		zipShot.locationlist = locationsList;
	//
	//		return zipShot;
	//	}

	//	private InterestModel parseInterest(JSONObject json) {
	//
	//		InterestModel obj = new InterestModel();
	//
	//		obj.id = json.optString("id");
	//		obj.name = json.optString("name");
	//
	//		return obj;
	//	}
	//	private FiltersModel parseFilters(JSONObject json) {
	//
	//		FiltersModel obj = new FiltersModel();
	//
	//		obj.id = json.optString("id");
	//		obj.title = json.optString("title");
	//		obj.current = json.optString("current");
	//		obj.type = json.optString("type");
	//		return obj;
	//	}
	//
	//
	//	private NeighbourDTO parseNeighbour(JSONObject json) {
	//
	//		NeighbourDTO obj = new NeighbourDTO();
	//
	//		obj.neighbhorhood = json.optString("neighbhorhood");
	//		obj.city = json.optString("city");
	//		obj.state= json.optString("state");
	//
	//		return obj;
	//	}
	private JSONObject insetJsonRequest(String desc, String imageName,
			int interestId, String zipcode, String place) {
		JSONObject json = new JSONObject();
		try {
			//Utility.logToFile(ctx, " insetJsonRequest::insertZIP  ", "MESSAGE");
			json.put("description", desc);
			json.put("imageName", imageName);
			json.put("interest_id", interestId);
			json.put("zipcode", zipcode);
			json.put("place", place);

		} catch (JSONException e) {
			e.printStackTrace();
			//Utility.logToFile(ctx, " insetJsonRequest::insertZIP  " + e.getMessage(), "ERROR");
		}

		return json;
	}

	private JSONObject insertCommentRequest(String id, String comment) {
		JSONObject json = new JSONObject();

		try {
			json.put("id", id);
			json.put("comment", comment);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	private JSONObject insertChatPostRequest(String title, String des,String categoryId, String categoryName,String zipcode) {
		JSONObject json = new JSONObject();

		try {
			json.put("catid", categoryId);
			json.put("catname", categoryName);
			json.put("zipcode", zipcode);
			json.put("title", title);
			json.put("message", des);



		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}


	private JSONObject abuseRequest(String id, String description) {
		JSONObject json = new JSONObject();

		try {
			json.put("zipshotid", id);
			json.put("description", description);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	private JSONObject jsonLogIn(String email, String password) {
		JSONObject json = new JSONObject();

		try {
			json.put("username", email);
			json.put("password", password);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	private JSONObject jsonForgotPassword(String email) {
		JSONObject json = new JSONObject();

		try {
			json.put("email", email);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	private JSONObject jsonFB(String fbID, String fbToken) {
		JSONObject jsonObj = new JSONObject();

		try {
			jsonObj.put("agree" , "1");
			jsonObj.put("facebook_id", fbID);
			jsonObj.put("facebook_token", fbToken);
			jsonObj.put("zipcode" , Consts.selectedZipcode);
			jsonObj.put("subscribe" , "1");

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return jsonObj;
	}


	// =====================================================================

	public String  likePost(String postId, String token) {

		String response = "";
		String likes = "";
		String value = "";
		try {

			String url = Consts.LIKE_ZIPSHOT + postId + "&token=" + token
					+ "&json=1";

			response = server.postJSONObject(url, new JSONObject());

			if (response != null) {
				JSONObject jsonResJsonObject = new JSONObject(response);
				likes = jsonResJsonObject.optString("likes");
				value = jsonResJsonObject.optString("value");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return likes + ":" + value;
	}
	public String  likePostForCategory(String postId, String token) {

		String response = "";
		String likes = "";
		String value = "";
		try {

			String url = Consts.LIKE_ZIPCHATS + postId + "&token=" + token
					+ "&json=1";

			response = server.postJSONObject(url, new JSONObject());

			if (response != null) {
				JSONObject jsonResJsonObject = new JSONObject(response);
				likes = jsonResJsonObject.optString("likes");
				value = jsonResJsonObject.optString("value");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return likes + ":" + value;
	}
	//Event Like =====================================================================

	public String  likePostEvent(String postId, String token) {

		String response = "";
		String likes = "";
		String value = "";
		try {

			String url = Consts.LIKE_EVENTS + postId + "&token=" + token
					+ "&json=1";

			response = server.postJSONObject(url, new JSONObject());

			if (response != null) {
				JSONObject jsonResJsonObject = new JSONObject(response);
				likes = jsonResJsonObject.optString("likes");
				value = jsonResJsonObject.optString("value");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return likes + ":" + value;
	}


	public String deletePost(String postId, String token) {

		String response = null;

		try {

			// http://yellowzip.com/api/zipshots/delete/111?token=kh8g39aetk9hju02mi5kdr3u62&json=1

			String url = Consts.DELETE_ZIPSHOT + postId + "?token=" + token
					+ "&json=1";

			// String url = "http://yellowzip.com/api/zipshots/delete?i" +
			// postId + "&token=" + token
			// + "&json=1";

			// response = server.postJSONObject(url,
			// deleteRequest(postId));

			response = server.postJSONObject(url, new JSONObject());

			if (response != null) {

				System.out.println("Delete Response ===== "
						+ response.toString());

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}

	private JSONObject deleteRequest(String id) {
		JSONObject json = new JSONObject();

		try {
			json.put("id", id);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}




	private String readJSONFeedWITHPOST(String urlString,StringEntity entity) 
	{
		StringBuilder builder = new StringBuilder();
		HttpClient httpclient;
		HttpPost httppost;

		httpclient = new DefaultHttpClient();
		httppost = new HttpPost(urlString);





		try {
			//	httppost.setEntity(new UrlEncodedFormEntity(postParameters));
			httppost.setEntity(entity);
			HttpResponse response = httpclient.execute(httppost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) 
			{
				HttpEntity entity2 = response.getEntity();
				InputStream content = entity2.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) 
				{
					builder.append(line);
				}
			} 
			else 
			{
				//Log.e(NetworkController.class.toString(), "Failed to download file");
				HttpEntity entity2 = response.getEntity();
				InputStream content = entity2.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) 
				{
					builder.append(line);
				}
			}
		} 
		catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}


	private String readJSONFeed(String urlString) 
	{
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(urlString);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				//Log.e(ctx, "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}


}
