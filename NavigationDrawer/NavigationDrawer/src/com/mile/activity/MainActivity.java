/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mile.activity;

import android.R.drawable;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.mile.android.R;
import com.mile.fragments.AboutMileFragment;
import com.mile.fragments.DocumentFragment;
import com.mile.fragments.GalleryFragment;
import com.mile.fragments.LogOutFragment;
import com.mile.fragments.MainMenuFragment;
import com.mile.fragments.MainSpecialDocumentFragment;
import com.mile.fragments.NotificationFragment;
import com.mile.fragments.ProgramFragment;
import com.mile.fragments.ProgramNotificationFragment;
import com.mile.fragments.SendNotificationFragment;
import com.mile.utility.Consts;

public class MainActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    static DocumentFragment planet;
    ProgramFragment mercury;
    GalleryFragment gallery;
    LogOutFragment logoutFragment;
    SendNotificationFragment sendNotificationFragment;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mTitle = mDrawerTitle = getTitle();
        if(Consts.Current_User.is_admin.equalsIgnoreCase("1"))
    		
		{
        	mPlanetTitles = getResources().getStringArray(R.array.fragments_array);
		}
		else
		{
			mPlanetTitles = getResources().getStringArray(R.array.planets_array);
		}
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
		planet = new DocumentFragment();
		mercury = new ProgramFragment();
		gallery = new GalleryFragment();
		logoutFragment = new LogOutFragment();
		sendNotificationFragment= new SendNotificationFragment();
        //mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mPlanetTitles));
      //getActionBar().setBackgroundDrawable(new ColorDrawable(R.color.Yellow));
        
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //getActionBar().setIcon(R.drawable.androidicon);
        
        //getActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#550000ff")));       
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(Html.fromHtml("<font color=\"black\">" + "MILE" + "</font>"));
                
                invalidateOptionsMenu(); 
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(Html.fromHtml("<font color=\"black\">" + "MILE" + "</font>"));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
        String str = getIntent().getStringExtra("PushMessage");
		if (!str.isEmpty() && str !=null) {
			selectItem(2);
		}
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
        //case R.id.action_websearch:*/
            // create intent to perform web search for this planet
         /*   Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
            // catch event that there's no activity to handle intent
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
            }
            return true;*/
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null ;
        Bundle args = new Bundle();
        switch(position){
        case 1 :
        	fragment = new ProgramFragment();
        	args.putInt(ProgramFragment.ARG_MER_NUMBER, position);
            break;
        case 0 :
        	fragment = new MainMenuFragment();
        	args.putInt(MainMenuFragment.ARG_MAIN_NUMBER, position);
        	break;
        case 2 :
        	fragment = new NotificationFragment();
        	args.putInt(NotificationFragment.ARG_NOTIF_NUMBER, position);
        	Consts.isGalleryFirstTime = true;
        	break;
        case 3 :
        	fragment = new MainSpecialDocumentFragment();
        	args.putInt(MainSpecialDocumentFragment.ARG_SPECIAL_DOC_NUMBER, position);
        	Consts.isGalleryFirstTime = true;
        	break;
        case 4 :
        	fragment = new AboutMileFragment();
        	args.putInt(AboutMileFragment.ARG_ABOUT_NUMBER, position);
        	
        	
        	break;
        case 5 :
        	fragment = new LogOutFragment();
        	args.putInt(LogOutFragment.ARG_LOG_OUT_NUMBER, position);
        	
        	break;
            	
       
        
        }
       /* fragment = new ProgramFragment();
    	args.putInt(ProgramFragment.ARG_MER_NUMBER, position);
    	break;*/
        
        fragment.setArguments(args);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(Html.fromHtml("<font color=\"black\">" + "MILE" + "</font>"));
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(Html.fromHtml("<font color=\"black\">" + "MILE" + "</font>"));
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // your code
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}