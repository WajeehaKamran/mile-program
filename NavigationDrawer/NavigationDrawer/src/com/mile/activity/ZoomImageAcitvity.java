package com.mile.activity;

import com.mile.android.R;
import com.mile.utility.ImageLoader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class ZoomImageAcitvity extends Activity {

	public String toString() {
		return "ZoomImageAcitvity";
	}

	Button btnClose;
	ImageView imgZipShot;
	ImageLoader imageLoader;
	private TouchImageView image;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_zoom);
		hideSoftKeyboard();
		imageLoader=new ImageLoader(this,150);
		//btnClose = (Button)findViewById(R.id.neighbourhood_btn_close);
		
		image = (TouchImageView) findViewById(R.id.imageView1);

		Intent intent  = getIntent();
		Bundle extras = intent.getExtras();
		if(extras.getString("imageURL")!= null)
		{
			imageLoader.DisplayImage(extras.getString("imageURL"), image, true,0);
		}
		//btnClose.setOnClickListener(onCloseButtonClicked);
		
	}

@Override
public void onResume() {
	super.onResume();

}
public void hideSoftKeyboard() {
	if(getCurrentFocus()!=null) {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	}
}

private OnClickListener onCloseButtonClicked = new OnClickListener() {

	@Override
	public void onClick(View v) {
		finish();

	}
};



}