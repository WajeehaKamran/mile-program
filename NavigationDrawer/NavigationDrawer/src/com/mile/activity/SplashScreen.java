package com.mile.activity;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;

import com.mile.android.R;
import com.mile.bl.ZipshotBL;
import com.mile.utility.AlertDialogManager;
import com.mile.utility.AppPreferences;
import com.mile.utility.ConnectionDetector;
import com.mile.utility.Consts;
public class SplashScreen extends Activity {

	TimerTask splashTask;
	final Handler handler = new Handler();
	Timer t = new Timer();
	ConnectionDetector conec;
	ZipshotBL zsBL;
	AppPreferences prefs;

	private boolean isInternetPresent = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spalsh_screen);
		zsBL = new ZipshotBL(this);
		prefs = new AppPreferences();
		conec = new ConnectionDetector(this);
		//		new Handler().postDelayed(new Runnable() {
		//
		//			@Override
		//			public void run() {
		//				Intent i = new Intent(SplashScreen.this, LoginScreen.class);
		//				startActivity(i);
		//				finish();
		//			}
		//		}, Consts.SPLASH_TIME_OUT);
		isInternetPresent = conec.isConnectingToInternet();

		if (isInternetPresent) {
			onPerformLoginTask();
		} else {
			// Internet connection is not present
			// Ask user to connect to Internet
			AlertDialogManager.showAlertDialog(SplashScreen.this, "No Internet Connection",
					"You don't have internet connection.", false);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}
	private void onPerformLoginTask() {

		splashTask = new TimerTask() {
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						FileInputStream fIn = null;

						InputStreamReader isr = null;

						fIn = null;
						isr = null;

						try{

							char[] inputBuffer = new char[1024];

							String data = null;

							fIn = openFileInput("loginstate.dat");

							isr = new InputStreamReader(fIn);

							isr.read(inputBuffer);

							data = new String(inputBuffer);
							//String[] separated = data.split(";");

							if(data.length()>0)
								Consts.lastLoginState = data.trim();
							isr.close();

							fIn.close();

						}catch(IOException e){

							e.printStackTrace(System.err);

						}

						if(Consts.lastLoginState.equals("") )
						{
							Intent i = new Intent(SplashScreen.this,
									LoginScreen.class);
							startActivity(i);
							overridePendingTransition(R.anim.slide_in_right,
									R.anim.slide_out_left);
							SplashScreen.this.finish();
						}
						else
						{
							//fetch the UserData for Login
							try{

								char[] inputBuffer = new char[1024];

								String data = null;

								fIn = openFileInput("userData.dat");

								isr = new InputStreamReader(fIn);

								isr.read(inputBuffer);

								data = new String(inputBuffer);
								String[] separated = data.split(";");

								if(separated.length>1)
								{
									Consts.Current_User.email = separated[0].trim();
									Consts.Current_User.password = separated[1].trim();
									new LoginTask().execute();
								}

								isr.close();

								fIn.close();

							}catch(IOException e)
							{

								e.printStackTrace(System.err);

							}
						}

					}
				});
			}
		};
		t.schedule(splashTask, 3);
	}

	class LoginTask extends AsyncTask<Void, Void, Void> {

		String response = "";
		@Override

		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			response = zsBL.logInAPI(Consts.Current_User.email, Consts.Current_User.password);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);


			if(response.contains("success"))
			{
				Intent i = new Intent(SplashScreen.this,
						MainActivity.class);
				i.putExtra("PushMessage", "");

				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left); 
			}

		}
	}


}// end class