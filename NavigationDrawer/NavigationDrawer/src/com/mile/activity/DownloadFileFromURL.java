package com.mile.activity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class DownloadFileFromURL extends AsyncTask<String, String, String> {
	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	URL url;
	String fileName;
	private ProgressDialog mProgressDialog;
	private Context context;
	public static final int progress_bar_type = 0;
	public DownloadFileFromURL(Context context) 
	{
		this.context = context;
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage("Downloading file..");
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.setMax(100);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.setCancelable(true);

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog.show();
	}

	@Override
	protected String doInBackground(String... f_url) {
		int count;
		try {
			url = new URL(f_url[0]);
			fileName = f_url[1]; 
			URLConnection conection = url.openConnection();
			conection.connect();
			// getting file length
			int lenghtOfFile = conection.getContentLength();

			// input stream to read file - with 8k buffer
			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			// Output stream to write file
			OutputStream output = new FileOutputStream("/sdcard/"+fileName);

			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				// publishing the progress....
				// After this onProgressUpdate will be called
				publishProgress(""+(int)((total*100)/lenghtOfFile));

				// writing data to file
				output.write(data, 0, count);
			}

			// flushing output
			output.flush();

			// closing streams
			output.close();
			input.close();

		} catch (Exception e) {
			Log.e("Error: ", e.getMessage());
		}

		return null;
	}

	/**
	 * Updating progress bar
	 * */
	protected void onProgressUpdate(String... progress) {
		// setting progress percentage
		mProgressDialog.setProgress(Integer.parseInt(progress[0]));
	}

	/**
	 * After completing background task
	 * Dismiss the progress dialog
	 * **/
	@Override
	protected void onPostExecute(String file_url) {
		// dismiss the dialog after the file was downloaded
		mProgressDialog.dismiss();
		openDocument(fileName);

	}



	public void openDocument(String name)
	{
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+name);

		String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
		
		String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
		if (extension.equalsIgnoreCase("") || mimetype == null)
		{

			// if there is no extension or there is no definite mimetype, still try to open the file
			intent.setDataAndType(Uri.fromFile(file), "text/*");
		}
		else
		{
			intent.setDataAndType(Uri.fromFile(file), mimetype);
		}
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		context.startActivity(intent);
	}
}

