package com.mile.activity;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;
import com.mile.android.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mile.bl.ProfileBL;
import com.mile.bl.ZipshotBL;
import com.mile.pushNotifications.ApplicationConstants;
import com.mile.pushNotifications.HomeActivity;
import com.mile.pushNotifications.Utility;
import com.mile.utility.AppPreferences;
import com.mile.utility.ConnectionDetector;
import com.mile.utility.Consts;


public class LoginScreen extends Activity  implements OnKeyListener {

	ZipshotBL zsBL;
	EditText editUserName, editPassword;
	Button btnLogin,btnSkip;
	ProfileBL profileBL;
	RadioButton showPassword;
	ProgressDialog pd;
	AppPreferences prefs;
	private ProgressDialog dialog;
	ConnectionDetector cd;
	LinearLayout relativeLayoutFilter;
	private Button btnForgotPassword;
	Context mContext = LoginScreen.this;
	String username, password;
	Boolean showPasswordClicked = false;
	RequestParams params = new RequestParams();
	GoogleCloudMessaging gcmObj;
	Context applicationContext;
	String regId = "";
	String loginResponse;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	AsyncTask<Void, Void, String> createRegIdTask;

	public static final String REG_ID = "regId";
	public static final String EMAIL_ID = "eMailId";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (getIntent().getBooleanExtra("EXIT", false)) {
			finish();

		}
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		// For Push Notification
		applicationContext = getApplicationContext();
		
		//////////////////////////////////////	
		zsBL = new ZipshotBL(this);
		profileBL = new ProfileBL(this);
		dialog = new ProgressDialog(this);
		
		
		editUserName = (EditText) findViewById(R.id.editUserName);
		editPassword = (EditText) findViewById(R.id.editPassword);
		btnLogin = (Button) findViewById(R.id.btnLogin);
		btnSkip = (Button) findViewById(R.id.btn_skip);
		showPassword = (RadioButton)findViewById(R.id.activity_login_show_password);
		btnForgotPassword = (Button)findViewById(R.id.btnForgotPassword);
		cd = new ConnectionDetector(getApplicationContext());
		editPassword.setOnKeyListener(this);
		prefs = new AppPreferences();

		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.example.android.navigationdrawerexample", 
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}

		btnLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				performLogin();
				Consts.isLoginSkip = true;

			}
		});
		btnSkip.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(LoginScreen.this,AboutScreen.class);
				startActivity(i);
				
			}
		});
		btnForgotPassword.setOnClickListener(onForgotPasswordClicked);


		showPassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(!showPasswordClicked){
					editPassword.setInputType(InputType.TYPE_CLASS_TEXT);
					showPassword.setChecked(true);
					showPasswordClicked = true;
				}
				else{
					editPassword.setInputType(InputType.TYPE_CLASS_TEXT | 
							InputType.TYPE_TEXT_VARIATION_PASSWORD);
					editPassword.setSelection(editPassword.getText().length());
					showPassword.setChecked(false);
					showPasswordClicked = false;

				}
			}
		});
	}

	void performLogin()
	{
		InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);


		if (editUserName.getText().toString().trim().length() < 1
				|| editPassword.getText().toString().trim().length() < 1) {

			Toast.makeText(LoginScreen.this,
					"Please provide username and Password.",
					Toast.LENGTH_SHORT).show();

		} else {
			new LoginTask().execute();
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			new AlertDialog.Builder(LoginScreen.this)
			.setIcon(R.drawable.fail)
			.setTitle("Quit MilePrograms")
			.setMessage(
					"Do u really want to exit MilePrograms Application?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {

							Intent intent = new Intent(LoginScreen.this, LoginScreen.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							intent.putExtra("EXIT", true);
							startActivity(intent);

							finish();
						}

					}).setNegativeButton("No", null).show();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}


	class LoginTask extends AsyncTask<Void, Void, Void> {

		

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(pd!=null)
			{
				pd.dismiss();
			}
			
			pd = new ProgressDialog(LoginScreen.this);
			pd.setTitle("Loading...");
			pd.setMessage("Please wait.");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			if (cd.isConnectingToInternet()) {
				loginResponse = zsBL.logInAPI(editUserName.getText().toString().trim(),
						editPassword.getText().toString().trim());
			

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(pd!=null)
			{
				pd.dismiss();
			}
			if (loginResponse != null){
				if(loginResponse.equalsIgnoreCase("success"))
				{
					SharedPreferences pref = getSharedPreferences("UserDetails",
							Context.MODE_PRIVATE);
					String registrationId = pref.getString(REG_ID, "");
					if (!TextUtils.isEmpty(registrationId)) {
						handleLoginResponse(loginResponse);
						//RegisterUser();
					}
					else{
						RegisterUser();
					}}
				else
				{
					
					Toast.makeText(LoginScreen.this, "Invalid Username or Password", Toast.LENGTH_LONG).show();
				}}
			 

		}
	}


	 void showAlertDialog(Context context, String title, String message, Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);
		
		// Setting alert dialog icon
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	private void handleLoginResponse(String loginResult)
	{

		if(loginResult.contains("success"))
		{
			Consts.lastLoginState = Consts.LOGIN_STATE_LOCAL;
			Consts.isGalleryFirstTime = true;
			FileOutputStream fOut = null;

			OutputStreamWriter osw = null;

			try{

				fOut = openFileOutput("loginstate.dat", Context.MODE_PRIVATE);

				osw = new OutputStreamWriter(fOut);

				osw.write(Consts.lastLoginState);

				osw.close();

				fOut.close();

			}catch(Exception e){

				e.printStackTrace(System.err);

			}

			String user = editUserName.getText().toString().trim();
			String pass = editPassword.getText().toString().trim();

			fOut = null;

			osw = null;

			try{

				fOut = openFileOutput("userData.dat", Context.MODE_PRIVATE);

				osw = new OutputStreamWriter(fOut);

				osw.write(user+";"+pass);

				osw.close();

				fOut.close();

			}catch(Exception e){

				e.printStackTrace(System.err);

			}
			Intent i = new Intent(LoginScreen.this,
					MainActivity.class);
			i.putExtra("PushMessage", "");
	        startActivity(i);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left); 

		}
		else
		{
			new AlertDialog.Builder(this).setTitle("Login Failed").setMessage("Username or Password is Invalid. Please try again ").setIcon(R.drawable.fail).setNeutralButton("Ok", null).show();

		}

	}


	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_ENTER)
		{
			performLogin();
		}
		return false;
	}
	private String getUserNameString(String name){
		String userName = name.toLowerCase();
		return userName.substring(0,1).toUpperCase()+ userName.substring(1,userName.length()).toLowerCase();
	}
	private OnClickListener onForgotPasswordClicked = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent i = new Intent(LoginScreen.this,
					MainActivity.class);
			i.putExtra("PushMessage", "");
	        
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left); 
		}
	};
	
	// Push Notification Section
	public void RegisterUser() {
		String emailID = editUserName.getText().toString();
		registerInBackground(emailID);
		if (!TextUtils.isEmpty(emailID) && Utility.validate(emailID)) {
			if (checkPlayServices()) {
				registerInBackground(emailID);
			}
		}
		
		else {
			/*Toast.makeText(applicationContext, "",
					Toast.LENGTH_LONG).show();*/
		}

	}

	private void registerInBackground(final String emailID) {
		new AsyncTask<Void, Void, String>() {
			String response;
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcmObj == null) {
						gcmObj = GoogleCloudMessaging
								.getInstance(applicationContext);
					}
					regId = gcmObj
							.register(ApplicationConstants.GOOGLE_PROJ_ID);
					msg = "Registration ID :" + regId;

				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				if (!TextUtils.isEmpty(regId)) {
					storeRegIdinSharedPref(applicationContext, regId, emailID);
					
					
				} else {
					/*Toast.makeText(
							applicationContext,
							"Reg ID Creation Failed.\n\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time."
									+ msg, Toast.LENGTH_LONG).show();*/
				}
			}
		}.execute(null, null, null);
	}

	private void storeRegIdinSharedPref(Context context, String regId,
			String emailID) {
		SharedPreferences prefs = getSharedPreferences("UserDetails",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(REG_ID, regId);
		editor.putString(EMAIL_ID, emailID);
		editor.commit();
		storeRegIdinServer();

	}

	private void storeRegIdinServer() {
		new RegisterDeviceIdOnServersTask().execute(regId);
		

	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				
				finish();
			}
			return false;
		} else {
			
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		checkPlayServices();
	}
	class RegisterDeviceIdOnServersTask extends AsyncTask<String, Void, Void> {

		String response = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(LoginScreen.this);
			pd.setTitle("Loading...");
			pd.setMessage("");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected Void doInBackground(String... arg0) {
			if (cd.isConnectingToInternet()) {
				response = zsBL.registerDeviceIdAPI(editUserName.getText().toString().trim(),
						arg0[0]);
			

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if (response != null){
				if(response.equalsIgnoreCase("success"))
				{
					/*Toast.makeText(applicationContext,
							"Reg Id shared successfully with Web App ",
							Toast.LENGTH_LONG).show();*/
					handleLoginResponse(loginResponse);
					}}
				else
				{
					
						new AlertDialog.Builder(getApplicationContext()).setTitle("Not Registered")
						.setMessage(response)
						.setIcon(R.drawable.error_icon)
						.setNeutralButton("Ok", null).show();
						if(pd!=null)
						{
							pd.dismiss();
						}
				}}

		
	}


}
