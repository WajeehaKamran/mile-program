package com.mile.model;

import java.util.ArrayList;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public class DocumentModel {

	public String id;
	public String name;
	public String profilImage;
	Bitmap image;
	String interestTitle;
	String userid;
	public String documentLists;
	public int count;
	public String userId;
	public String docTitle;
	public ProgramListModel proglistObj;
	public String emailAddress;
	public ArrayList<ProgramListModel> documentList;
	
		public DocumentModel(int imageId, String title, Resources res, String int_id, int int_count) {
			super();
			this.image = BitmapFactory.decodeResource(res, imageId);
			this.interestTitle = title;
			this.name = title;
			this.id = int_id;
			this.count = int_count;
		}
		public DocumentModel() {
			
		}
		public Bitmap getImage() {
			return image;
		}
		public void setImage(Bitmap image) {
			this.image = image;
		}
		public String getTitle() {
			return interestTitle;
		}
		public void setTitle(String title) {
			this.interestTitle = title;
		}
		
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
	

}